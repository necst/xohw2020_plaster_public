#include <boost/python.hpp>
#include "PlasterAPI.h"
#include "../controller/Task.h"
#include "TaskWrapper.h"

using namespace boost::python;

BOOST_PYTHON_MODULE (pyPlaster) {
    class_<PlasterAPI, boost::noncopyable>("PlasterAPI")
            .def("connect", &PlasterAPI::connect)
            .def("launchApp", &PlasterAPI::launchApp)
            .def("killApp", &PlasterAPI::killApp)
            .def("restartApp", &PlasterAPI::restartApp)
            .def("setOutputCollection", &PlasterAPI::setOutputCollection)
            .def("consumeOutput", &PlasterAPI::consumeOutput)
            .def("transfer_file", &PlasterAPI::transferFile);
}
