#include "StartAppCommand.h"

#include <utility>
#include "ApiCommandsVisitor.h"

BOOST_CLASS_EXPORT_IMPLEMENT(StartAppCommand)

void StartAppCommand::accept(ApiCommandsVisitor &visitor) const {
    visitor.handle((StartAppCommand &) *this);
}

StartAppCommand::StartAppCommand(std::string appLocation) : appLocation(std::move(appLocation)) {}

std::string &StartAppCommand::getAppLocation() {
    return appLocation;
}