#ifndef PLASTER_RESTARTAPPCOMMAND_H
#define PLASTER_RESTARTAPPCOMMAND_H

#include "ApiCommand.h"
#include <boost/serialization/export.hpp>

/**
 * A specialized API command that represents the request to restart an app in the system.
 * It holds information about the current identifier of the app within the system
 * and about the path of the app's source files, provided by the user.
 */
class RestartAppCommand : public ApiCommand {

public:

    RestartAppCommand() = default;

    explicit RestartAppCommand(std::string appId, std::string appLocation);

    std::string &getAppId();

    std::string &getAppLocation();

    void accept(ApiCommandsVisitor &visitor) const override;

private:

    friend class boost::serialization::access;

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar.template register_type<RestartAppCommand>();
        ar & boost::serialization::base_object<ApiCommand>(*this);
        ar & appId;
        ar & appLocation;
    }

    std::string appId;

    std::string appLocation;
};

BOOST_CLASS_EXPORT_KEY(RestartAppCommand)


#endif //PLASTER_RESTARTAPPCOMMAND_H
