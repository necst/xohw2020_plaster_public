#ifndef PLASTER_KILLAPPCOMMAND_H
#define PLASTER_KILLAPPCOMMAND_H

#include "ApiCommand.h"
#include <boost/serialization/export.hpp>

/**
 * A specialized API command that represents the request to stop an app in the system.
 * It holds information about the identifier of the app within the system.
 */
class KillAppCommand : public ApiCommand {

public:

    KillAppCommand() = default;

    explicit KillAppCommand(std::string appId);

    std::string &getAppId();

    void accept(ApiCommandsVisitor &visitor) const override;

private:

    friend class boost::serialization::access;

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar.template register_type<KillAppCommand>();
        ar & boost::serialization::base_object<ApiCommand>(*this);
        ar & appId;
    }

    std::string appId;
};

BOOST_CLASS_EXPORT_KEY(KillAppCommand)

#endif //PLASTER_KILLAPPCOMMAND_H
