#include "TransferFileCommand.h"
#include "ApiCommandsVisitor.h"

BOOST_CLASS_EXPORT_IMPLEMENT(TransferFileCommand)

TransferFileCommand::TransferFileCommand(std::string owner, std::string filePath, std::string fileIdentifier) :
        owner(owner), filePath(filePath), fileIdentifier(fileIdentifier) {
}

std::string &TransferFileCommand::getOwner() {
    return owner;
}

std::string &TransferFileCommand::getFilePath() {
    return filePath;
}

std::string &TransferFileCommand::getFileIdentifier() {
    return fileIdentifier;
}

void TransferFileCommand::accept(ApiCommandsVisitor &visitor) const {
    visitor.handle((TransferFileCommand &) *this);
}