#include "ConsumeOutputCommand.h"

#include <utility>
#include "ApiCommandsVisitor.h"

BOOST_CLASS_EXPORT_IMPLEMENT(ConsumeOutputCommand)

void ConsumeOutputCommand::accept(ApiCommandsVisitor &visitor) const {
    visitor.handle((ConsumeOutputCommand &) *this);
}

ConsumeOutputCommand::ConsumeOutputCommand(std::string appId) : appId(std::move(appId)) {}

std::string &ConsumeOutputCommand::getAppId() {
    return appId;
}
