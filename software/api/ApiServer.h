#ifndef PLASTER_APISERVER_H
#define PLASTER_APISERVER_H

#include <zmq.hpp>
#include <thread>

#include "ApiCommand.h"
#include "../utils/Observable.h"

/**
 * A class representing an object used to provide endpoints for external APIs.
 * It wraps a ZeroMQ socket connection and can be observed by other modules,
 * which are notified about incoming API commands.
 */
class ApiServer : public Observable<ApiCommand> {

public:

    /**
     * Creates an instance of the server, sets up the socket connection and
     * starts listening for incoming requests
     */
    ApiServer();

    /**
     * Sets whether the server is running or not
     * @param isRunning {@code true} if the server is running, {@code false} otherwise
     */
    void setIsRunning(bool isRunning);

    /**
     * Sends the provided data to the attached client
     * @param data the textual representation of the data to send, not null
     */
    void sendData(const std::string &data);

private:
    zmq::context_t context{1};
    zmq::socket_t socket{context, ZMQ_REP};

    bool isRunning = true;

    /**
     * The reference to the thread with which incoming messages are listened
     */
    std::thread listenerTask;

    void runner();
};


#endif //PLASTER_APISERVER_H
