#ifndef PLASTER_SETOUTCOLLECTIONCOMMAND_H
#define PLASTER_SETOUTCOLLECTIONCOMMAND_H

#include "ApiCommand.h"
#include <boost/serialization/export.hpp>

/**
 * A specialized API command that represents the request to set the flag for output collection of an app in the system.
 * It holds information about the identifier of the app within the system.
 */
class SetOutCollectionCommand : public ApiCommand {

public:

    SetOutCollectionCommand() = default;

    explicit SetOutCollectionCommand(std::string appId, bool isOutputCollected);

    std::string &getAppId();

    bool isOutputCollected();

    void accept(ApiCommandsVisitor &visitor) const override;

private:

    friend class boost::serialization::access;

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar.template register_type<SetOutCollectionCommand>();
        ar & boost::serialization::base_object<ApiCommand>(*this);
        ar & appId;
        ar & outputCollected;
    }

    std::string appId;

    bool outputCollected;
};

BOOST_CLASS_EXPORT_KEY(SetOutCollectionCommand)


#endif //PLASTER_SETOUTCOLLECTIONCOMMAND_H
