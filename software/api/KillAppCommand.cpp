#include "KillAppCommand.h"

#include <utility>
#include "ApiCommandsVisitor.h"

BOOST_CLASS_EXPORT_IMPLEMENT(KillAppCommand)

void KillAppCommand::accept(ApiCommandsVisitor &visitor) const {
    visitor.handle((KillAppCommand &) *this);
}

KillAppCommand::KillAppCommand(std::string appId) : appId(std::move(appId)) {}

std::string &KillAppCommand::getAppId() {
    return appId;
}
