#include "RestartAppCommand.h"

#include <utility>
#include "ApiCommandsVisitor.h"

BOOST_CLASS_EXPORT_IMPLEMENT(RestartAppCommand)

void RestartAppCommand::accept(ApiCommandsVisitor &visitor) const {
    visitor.handle((RestartAppCommand &) *this);
}

RestartAppCommand::RestartAppCommand(std::string appId, std::string appLocation) :
        appId(std::move(appId)),
        appLocation(std::move(appLocation)) {}

std::string &RestartAppCommand::getAppId() {
    return appId;
}

std::string &RestartAppCommand::getAppLocation() {
    return appLocation;
}
