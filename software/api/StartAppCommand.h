#ifndef PLASTER_STARTAPPCOMMAND_H
#define PLASTER_STARTAPPCOMMAND_H

#include "ApiCommand.h"
#include <boost/serialization/export.hpp>

/**
 * A specialized API command that represent the request to start an app in the system.
 * It holds information about the path of the app's source files, provided by the user.
 */
class StartAppCommand : public ApiCommand {

public:

    StartAppCommand() = default;

    explicit StartAppCommand(std::string appLocation);

    std::string &getAppLocation();

    void accept(ApiCommandsVisitor &visitor) const override;

private:

    friend class boost::serialization::access;

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar.template register_type<StartAppCommand>();
        ar & boost::serialization::base_object<ApiCommand>(*this);
        ar & appLocation;
    }

    std::string appLocation;
};

BOOST_CLASS_EXPORT_KEY(StartAppCommand)


#endif //PLASTER_STARTAPPCOMMAND_H