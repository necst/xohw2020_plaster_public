#ifndef PLASTER_TASKWRAPPER_H
#define PLASTER_TASKWRAPPER_H

#include <boost/python.hpp>
#include "../controller/Task.h"

using namespace boost::python;

/**
 * A class representing an object that wraps the Task class to be used and extended in a Python environment
 */
class TaskWrapper final : public Task, public wrapper<Task> {
public:
    TaskWrapper() : Task() {}

    void setup() override {
        this->get_override("setup")();
    }

    void run() override {
        this->get_override("run")();
    }

    void onMessageReceived(std::string sender, std::string messageType, std::string messagePayload) override {
        this->get_override("onMessageReceived")(sender, messageType, messagePayload);
    }
};

#endif //PLASTER_TASKWRAPPER_H
