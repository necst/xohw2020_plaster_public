#ifndef PLASTER_PLASTERAPI_H
#define PLASTER_PLASTERAPI_H

#define SERVER_ADDR "tcp://localhost:5555"

#include <string>
#include <list>
#include <iostream>
#include <thread>
#include <utility>
#include <zmq.hpp>
#include "StartAppCommand.h"
#include "KillAppCommand.h"
#include "RestartAppCommand.h"
#include "SetOutCollectionCommand.h"
#include "ConsumeOutputCommand.h"
#include "TransferFileCommand.h"

/**
 * A class representing an object that provides APIs to external systems to interact with the cluster.
 * It provides methods to connect and disconnect to a local node manager and perform operations on it.
 */
class PlasterAPI {
public:

    PlasterAPI() = default;

    /**
     * Connects to the local node manager
     */
    void connect() {
        socket.connect(SERVER_ADDR);
        std::cout << "Connected" << std::endl;
    } //TODO: Check whether to return if the connection was successful

    /**
     * Closes the connection to the local node manager
     */
    void disconnect() {
        socket.disconnect(SERVER_ADDR);
    }

    /**
     * Launches an app in the system, executing its source files located at the provided location
     * @param appLocation the path of the source files of the app, not empty
     * @return a string containing the identifier of the app within the system
     */
    std::string launchApp(std::string appLocation) {

        if (appLocation.empty()) {
            throw std::invalid_argument("App location cannot be empty");
        }

        // Create a string stream to put the serialized object
        std::stringstream ss;
        // Create a new object representing a command to start the app
        ApiCommand *command = new StartAppCommand(appLocation);
        boost::archive::text_oarchive oa(ss);
        // Then serialize it
        oa << command;
        // And send it to the local node manager through the socket connection
        zmq::message_t request(ss.str().length());
        memcpy(request.data(), ss.str().c_str(), ss.str().length());
        socket.send(request);
        // Then wait for the reply
        zmq::message_t reply;
        socket.recv(&reply);
        // Extract the content
        std::string replyData = std::string(static_cast<char *>(request.data()), request.size());
        // And return it to the caller
        return replyData;
    }

    /**
     * Stops the execution of the provided app in the system
     * @param appId the identifier of the app within the system, not empty
     * @return {@code true} if the app was successfully stopped, {@code false} otherwise
     */
    bool killApp(std::string appId) {

        if (appId.empty()) {
            throw std::invalid_argument("App identifier cannot be empty");
        }

        // Create a string stream to put the serialized object
        std::stringstream ss;
        // Create a new object representing a command to kill the app
        ApiCommand *command = new KillAppCommand(appId);
        boost::archive::text_oarchive oa(ss);
        // Then serialize it
        oa << command;
        // And send it to the local node manager through the socket connection
        zmq::message_t request(ss.str().length());
        memcpy(request.data(), ss.str().c_str(), ss.str().length());
        socket.send(request);
        // Then wait for the reply
        zmq::message_t reply;
        socket.recv(&reply);
        // Extract the content
        bool replyData = static_cast<bool>(request.data()); // TODO: Check if it's correct
        // And return it to the caller
        return replyData;
        // TODO: Implement this method
    }

    /**
     * Restarts the execution of the provided app
     * @param appId the identifier of the app within the system, not empty
     * @param appLocation the path of the source files of the app, not empty
     * @return a string containing the new identifier of the app within the system
     */
    std::string restartApp(std::string appId, std::string appLocation) {

        if (appLocation.empty() || appId.empty()) {
            throw std::invalid_argument("Found empty parameters");
        }

        // Create a string stream to put the serialized object
        std::stringstream ss;
        // Create a new object representing a command to restart the app
        ApiCommand *command = new RestartAppCommand(appId, appLocation);
        boost::archive::text_oarchive oa(ss);
        // Then serialize it
        oa << command;
        // And send it to the local node manager through the socket connection
        zmq::message_t request(ss.str().length());
        memcpy(request.data(), ss.str().c_str(), ss.str().length());
        socket.send(request);
        // Then wait for the reply
        zmq::message_t reply;
        socket.recv(&reply);
        // Extract the content
        std::string replyData = std::string(static_cast<char *>(request.data()), request.size());
        // And return it to the caller
        return replyData;
    }

    /**
     * Sets the flag to collect output from the provided app
     * @param appId the identifier of the app within the system, not empty
     * @param isOutputCollected {@code true} if the output should be collected, {@code false} otherwise
     * @return {@code true} if the flag was set successfully, {@code false} otherwise
     */
    bool setOutputCollection(std::string appId, bool isOutputCollected) {

        if (appId.empty()) {
            throw std::invalid_argument("App identifier cannot be empty");
        }

        // Create a string stream to put the serialized object
        std::stringstream ss;
        // Create a new object representing a command to start the app
        ApiCommand *command = new SetOutCollectionCommand(appId, isOutputCollected);
        boost::archive::text_oarchive oa(ss);
        // Then serialize it
        oa << command;
        // And send it to the local node manager through the socket connection
        zmq::message_t request(ss.str().length());
        memcpy(request.data(), ss.str().c_str(), ss.str().length());
        socket.send(request);
        // Then wait for the reply
        zmq::message_t reply;
        socket.recv(&reply);
        // Extract the content
        bool replyData = static_cast<bool>(request.data()); //TODO: Check if it's correct
        // And return it to the caller
        return replyData;
    }

    /**
     * Consumes the output generated by the provided app
     * @param appId the identifier of the app within the system, not empty
     * @return the list of strings containing the output generated by the app
     */
    std::list<std::string> consumeOutput(std::string appId) {

        if (appId.empty()) {
            throw std::invalid_argument("App identifier cannot be empty");
        }

        // Create a string stream to put the serialized object
        std::stringstream ss;
        // Create a new object representing a command to start the app
        ApiCommand *command = new ConsumeOutputCommand(appId);
        boost::archive::text_oarchive oa(ss);
        // Then serialize it
        oa << command;
        // And send it to the local node manager through the socket connection
        zmq::message_t request(ss.str().length());
        memcpy(request.data(), ss.str().c_str(), ss.str().length());
        socket.send(request);
        // Then wait for the reply
        zmq::message_t reply;
        socket.recv(&reply);
        std::list<std::string> output{};
        // TODO: Check how to retrieve the received output (and how to represent it)
        return output;
    }

    void transferFile(std::string owner, std::string filePath, std::string fileIdentifier) {
        std::stringstream ss;
        // Create a new object representing a command to start the app
        ApiCommand *command = new TransferFileCommand(owner, filePath, fileIdentifier);
        boost::archive::text_oarchive oa(ss);
        // Then serialize it
        oa << command;
        // And send it to the local node manager through the socket connection
        zmq::message_t request(ss.str().length());
        memcpy(request.data(), ss.str().c_str(), ss.str().length());
        socket.send(request);
        // Then wait for the reply
        zmq::message_t reply;
        socket.recv(&reply);
    }

private:
    zmq::context_t context{1};
    zmq::socket_t socket{context, ZMQ_REQ};
};


#endif //PLASTER_PLASTERAPI_H
