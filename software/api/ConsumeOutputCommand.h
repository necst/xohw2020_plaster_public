#ifndef PLASTER_CONSUMEOUTPUTCOMMAND_H
#define PLASTER_CONSUMEOUTPUTCOMMAND_H

#include "ApiCommand.h"
#include <boost/serialization/export.hpp>

/**
 * A specialized API command that represents the request to consume the output collected by an app in the system.
 * It holds information about the identifier of the app within the system.
 */
class ConsumeOutputCommand : public ApiCommand {

public:

    ConsumeOutputCommand() = default;

    explicit ConsumeOutputCommand(std::string appId);

    std::string &getAppId();

    void accept(ApiCommandsVisitor &visitor) const override;

private:

    friend class boost::serialization::access;

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar.template register_type<ConsumeOutputCommand>();
        ar & boost::serialization::base_object<ApiCommand>(*this);
        ar & appId;
    }

    std::string appId;

    // TODO: Check whether to keep other information to later send the output
};

BOOST_CLASS_EXPORT_KEY(ConsumeOutputCommand)


#endif //PLASTER_CONSUMEOUTPUTCOMMAND_H
