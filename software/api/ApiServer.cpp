#define SERVER_ADDRESS "tcp://*:5555"

#include "ApiServer.h"
#include <iostream>

ApiServer::ApiServer() {
    std::cout << "API_SERVER API socket started" << std::endl;
    // Set up the listener thread
    listenerTask = std::thread(&ApiServer::runner, this);
}

void ApiServer::runner() {
    // First bind the socket to the specified port
    try {
        socket.bind(SERVER_ADDRESS);
    }
    catch (std::exception &e) {
        // If a server is already present, we do nothing
        std::cout << e.what() << std::endl;
        return;
    }
    // Then start listening
    while (this->isRunning) {
        // Create an object to hold the request
        zmq::message_t request;
        // And wait for it
        socket.recv(&request);
        // Once we received the message we parse its content
        std::string requestData = std::string(static_cast<char *>(request.data()), request.size());
        // Create a new ApiCommand object
        ApiCommand *apiCommand;
        // Deserialize the incoming message to it
        std::stringstream ss(requestData);
        text_iarchive ia{ss};
        ia >> apiCommand;
        // And then notify observers about the command
        notifyObservers(*apiCommand);
        std::cout << "received message" << std::endl;
        // Then sleep before the next step
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        // Send an ack to the client
        sendData("ack");
    }
}

void ApiServer::setIsRunning(bool isRunning) {
    this->isRunning = isRunning;
}

void ApiServer::sendData(const std::string &data) {
    if (data.empty()) {
        throw std::invalid_argument("Data to send cannot be empty");
    }
    // Create an object holding the message
    zmq::message_t reply(data.length());
    // Put the provided data inside
    memcpy(reply.data(), data.c_str(), data.length());
    // And eventually send the message
    socket.send(reply);
}
