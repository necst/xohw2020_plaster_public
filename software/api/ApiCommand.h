#ifndef PLASTER_APICOMMAND_H
#define PLASTER_APICOMMAND_H

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/assume_abstract.hpp>
#include <iostream>
#include <sstream>

using namespace boost::archive;

// Forward declaration of visitor
class ApiCommandsVisitor;

/**
 * An abstract class that represents a command that has to be executed in response to an API call.
 * It will be handled by using a visitor pattern.
 */
class ApiCommand {
public:
    ApiCommand() = default;

    virtual void accept(ApiCommandsVisitor &visitor) const = 0;

private:
    friend class boost::serialization::access;

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar;
    };

};

BOOST_SERIALIZATION_ASSUME_ABSTRACT(ApiCommand)

#endif //PLASTER_APICOMMAND_H
