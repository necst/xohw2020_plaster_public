#ifndef PLASTER_TRANSFERFILECOMMAND_H
#define PLASTER_TRANSFERFILECOMMAND_H

#include "ApiCommand.h"
#include <boost/serialization/export.hpp>

class TransferFileCommand : public ApiCommand {

public:
    TransferFileCommand() = default;

    explicit TransferFileCommand(std::string owner, std::string filePath, std::string fileIdentifier);

    std::string &getOwner();

    std::string &getFilePath();

    std::string &getFileIdentifier();

    void accept(ApiCommandsVisitor &visitor) const override;

private:
    friend class boost::serialization::access;

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar.template register_type<TransferFileCommand>();
        ar & boost::serialization::base_object<ApiCommand>(*this);
        ar & owner;
        ar & filePath;
        ar & fileIdentifier;
    }

    std::string owner;
    std::string filePath;
    std::string fileIdentifier;

};

BOOST_CLASS_EXPORT_KEY(TransferFileCommand)

#endif