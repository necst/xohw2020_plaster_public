#ifndef PLASTER_APICOMMANDSVISITOR_H
#define PLASTER_APICOMMANDSVISITOR_H

class StartAppCommand;

class KillAppCommand;

class RestartAppCommand;

class SetOutCollectionCommand;

class ConsumeOutputCommand;

class TransferFileCommand;

/**
 * A class describing an object that implements the visitor pattern to handle the commands coming from the APIs
 */
class ApiCommandsVisitor {

public:
    virtual void handle(StartAppCommand &) = 0;

    virtual void handle(KillAppCommand &) = 0;

    virtual void handle(RestartAppCommand &) = 0;

    virtual void handle(SetOutCollectionCommand &) = 0;

    virtual void handle(ConsumeOutputCommand &) = 0;

    virtual void handle(TransferFileCommand &) = 0;
};

#endif //PLASTER_APICOMMANDSVISITOR_H
