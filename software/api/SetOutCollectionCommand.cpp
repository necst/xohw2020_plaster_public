#include "SetOutCollectionCommand.h"

#include <utility>
#include "ApiCommandsVisitor.h"

BOOST_CLASS_EXPORT_IMPLEMENT(SetOutCollectionCommand)

void SetOutCollectionCommand::accept(ApiCommandsVisitor &visitor) const {
    visitor.handle((SetOutCollectionCommand &) *this);
}

SetOutCollectionCommand::SetOutCollectionCommand(std::string appId, const bool isOutputCollected) :
        appId(std::move(appId)), outputCollected(isOutputCollected) {}

std::string &SetOutCollectionCommand::getAppId() {
    return appId;
}

bool SetOutCollectionCommand::isOutputCollected() {
    return outputCollected;
}
