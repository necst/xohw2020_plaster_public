# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.16

CMakeFiles/plaster.dir/Main.cpp.o: ../Main.cpp
CMakeFiles/plaster.dir/Main.cpp.o: ../Main.h
CMakeFiles/plaster.dir/Main.cpp.o: ../api/ApiCommand.h
CMakeFiles/plaster.dir/Main.cpp.o: ../api/ApiCommandsVisitor.h
CMakeFiles/plaster.dir/Main.cpp.o: ../api/ApiServer.h
CMakeFiles/plaster.dir/Main.cpp.o: ../api/ConsumeOutputCommand.h
CMakeFiles/plaster.dir/Main.cpp.o: ../api/KillAppCommand.h
CMakeFiles/plaster.dir/Main.cpp.o: ../api/PlasterAPI.h
CMakeFiles/plaster.dir/Main.cpp.o: ../api/RestartAppCommand.h
CMakeFiles/plaster.dir/Main.cpp.o: ../api/SetOutCollectionCommand.h
CMakeFiles/plaster.dir/Main.cpp.o: ../api/StartAppCommand.h
CMakeFiles/plaster.dir/Main.cpp.o: ../api/TransferFileCommand.h
CMakeFiles/plaster.dir/Main.cpp.o: ../controller/AppLauncher.h
CMakeFiles/plaster.dir/Main.cpp.o: ../controller/BinaryRepliesManager.h
CMakeFiles/plaster.dir/Main.cpp.o: ../controller/BoardResourcesManager.h
CMakeFiles/plaster.dir/Main.cpp.o: ../controller/NetworkManagerDelegate.h
CMakeFiles/plaster.dir/Main.cpp.o: ../controller/NodeManager.h
CMakeFiles/plaster.dir/Main.cpp.o: ../controller/Task.h
CMakeFiles/plaster.dir/Main.cpp.o: ../controller/TaskAvailabilityManager.h
CMakeFiles/plaster.dir/Main.cpp.o: ../controller/TaskAvailabilityResult.h
CMakeFiles/plaster.dir/Main.cpp.o: ../controller/TaskLaunchOutcome.h
CMakeFiles/plaster.dir/Main.cpp.o: ../controller/TaskLaunchOutcomeManager.h
CMakeFiles/plaster.dir/Main.cpp.o: ../controller/TaskView.h
CMakeFiles/plaster.dir/Main.cpp.o: ../events/EventsVisitor.h
CMakeFiles/plaster.dir/Main.cpp.o: ../events/NodeAvailability.h
CMakeFiles/plaster.dir/Main.cpp.o: ../events/PlasterEvent.h
CMakeFiles/plaster.dir/Main.cpp.o: ../events/TaskCommunication.h
CMakeFiles/plaster.dir/Main.cpp.o: ../execution/TaskExecutor.h
CMakeFiles/plaster.dir/Main.cpp.o: ../network/NetworkManager.h
CMakeFiles/plaster.dir/Main.cpp.o: ../network/NodeIdMapper.h
CMakeFiles/plaster.dir/Main.cpp.o: ../network/RoutingTable.h
CMakeFiles/plaster.dir/Main.cpp.o: ../utils/Observable.h
CMakeFiles/plaster.dir/Main.cpp.o: ../utils/Observer.h

CMakeFiles/plaster.dir/api/ApiServer.cpp.o: ../api/ApiCommand.h
CMakeFiles/plaster.dir/api/ApiServer.cpp.o: ../api/ApiServer.cpp
CMakeFiles/plaster.dir/api/ApiServer.cpp.o: ../api/ApiServer.h
CMakeFiles/plaster.dir/api/ApiServer.cpp.o: ../utils/Observable.h
CMakeFiles/plaster.dir/api/ApiServer.cpp.o: ../utils/Observer.h

CMakeFiles/plaster.dir/api/ConsumeOutputCommand.cpp.o: ../api/ApiCommand.h
CMakeFiles/plaster.dir/api/ConsumeOutputCommand.cpp.o: ../api/ApiCommandsVisitor.h
CMakeFiles/plaster.dir/api/ConsumeOutputCommand.cpp.o: ../api/ConsumeOutputCommand.cpp
CMakeFiles/plaster.dir/api/ConsumeOutputCommand.cpp.o: ../api/ConsumeOutputCommand.h

CMakeFiles/plaster.dir/api/KillAppCommand.cpp.o: ../api/ApiCommand.h
CMakeFiles/plaster.dir/api/KillAppCommand.cpp.o: ../api/ApiCommandsVisitor.h
CMakeFiles/plaster.dir/api/KillAppCommand.cpp.o: ../api/KillAppCommand.cpp
CMakeFiles/plaster.dir/api/KillAppCommand.cpp.o: ../api/KillAppCommand.h

CMakeFiles/plaster.dir/api/RestartAppCommand.cpp.o: ../api/ApiCommand.h
CMakeFiles/plaster.dir/api/RestartAppCommand.cpp.o: ../api/ApiCommandsVisitor.h
CMakeFiles/plaster.dir/api/RestartAppCommand.cpp.o: ../api/RestartAppCommand.cpp
CMakeFiles/plaster.dir/api/RestartAppCommand.cpp.o: ../api/RestartAppCommand.h

CMakeFiles/plaster.dir/api/SetOutCollectionCommand.cpp.o: ../api/ApiCommand.h
CMakeFiles/plaster.dir/api/SetOutCollectionCommand.cpp.o: ../api/ApiCommandsVisitor.h
CMakeFiles/plaster.dir/api/SetOutCollectionCommand.cpp.o: ../api/SetOutCollectionCommand.cpp
CMakeFiles/plaster.dir/api/SetOutCollectionCommand.cpp.o: ../api/SetOutCollectionCommand.h

CMakeFiles/plaster.dir/api/StartAppCommand.cpp.o: ../api/ApiCommand.h
CMakeFiles/plaster.dir/api/StartAppCommand.cpp.o: ../api/ApiCommandsVisitor.h
CMakeFiles/plaster.dir/api/StartAppCommand.cpp.o: ../api/StartAppCommand.cpp
CMakeFiles/plaster.dir/api/StartAppCommand.cpp.o: ../api/StartAppCommand.h

CMakeFiles/plaster.dir/api/TransferFileCommand.cpp.o: ../api/ApiCommand.h
CMakeFiles/plaster.dir/api/TransferFileCommand.cpp.o: ../api/ApiCommandsVisitor.h
CMakeFiles/plaster.dir/api/TransferFileCommand.cpp.o: ../api/TransferFileCommand.cpp
CMakeFiles/plaster.dir/api/TransferFileCommand.cpp.o: ../api/TransferFileCommand.h

CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../controller/AppLauncher.cpp
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../controller/AppLauncher.h
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../controller/BinaryRepliesManager.h
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../controller/NetworkManagerDelegate.h
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../controller/Task.h
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../controller/TaskAvailabilityManager.h
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../controller/TaskAvailabilityResult.h
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../controller/TaskLaunchOutcome.h
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../controller/TaskLaunchOutcomeManager.h
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../controller/TaskView.h
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../events/AskForAvailability.h
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../events/LaunchTask.h
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../events/NodeAvailability.h
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../events/PlasterEvent.h
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../network/NetworkManager.h
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../network/NodeIdMapper.h
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../network/RoutingTable.h
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../utils/Observable.h
CMakeFiles/plaster.dir/controller/AppLauncher.cpp.o: ../utils/Observer.h

CMakeFiles/plaster.dir/controller/BinaryRepliesManager.cpp.o: ../controller/BinaryRepliesManager.cpp
CMakeFiles/plaster.dir/controller/BinaryRepliesManager.cpp.o: ../controller/BinaryRepliesManager.h
CMakeFiles/plaster.dir/controller/BinaryRepliesManager.cpp.o: ../controller/TaskView.h

CMakeFiles/plaster.dir/controller/BoardResourcesManager.cpp.o: ../controller/BoardResourcesManager.cpp
CMakeFiles/plaster.dir/controller/BoardResourcesManager.cpp.o: ../controller/BoardResourcesManager.h
CMakeFiles/plaster.dir/controller/BoardResourcesManager.cpp.o: ../controller/TaskView.h

CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../api/ApiCommand.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../api/ApiCommandsVisitor.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../api/ApiServer.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../api/ConsumeOutputCommand.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../api/KillAppCommand.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../api/RestartAppCommand.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../api/SetOutCollectionCommand.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../api/StartAppCommand.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../api/TransferFileCommand.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../controller/AppLauncher.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../controller/BinaryRepliesManager.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../controller/BoardResourcesManager.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../controller/NetworkManagerDelegate.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../controller/NodeManager.cpp
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../controller/NodeManager.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../controller/Task.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../controller/TaskAvailabilityManager.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../controller/TaskAvailabilityResult.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../controller/TaskLaunchOutcome.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../controller/TaskLaunchOutcomeManager.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../controller/TaskView.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../events/AbortLaunchTask.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../events/AskForAvailability.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../events/CollectOutput.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../events/EventsVisitor.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../events/FileChunkEvent.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../events/FileRequestEvent.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../events/KillApp.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../events/LaunchCompleted.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../events/LaunchTask.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../events/NodeAvailability.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../events/PlasterEvent.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../events/SendOutput.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../events/TaskCommunication.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../execution/TaskExecutor.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../network/NetworkManager.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../network/NodeIdMapper.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../network/RoutingTable.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../utils/Observable.h
CMakeFiles/plaster.dir/controller/NodeManager.cpp.o: ../utils/Observer.h

CMakeFiles/plaster.dir/controller/Task.cpp.o: ../controller/Task.cpp
CMakeFiles/plaster.dir/controller/Task.cpp.o: ../controller/Task.h

CMakeFiles/plaster.dir/controller/TaskAvailabilityManager.cpp.o: ../controller/BinaryRepliesManager.h
CMakeFiles/plaster.dir/controller/TaskAvailabilityManager.cpp.o: ../controller/TaskAvailabilityManager.cpp
CMakeFiles/plaster.dir/controller/TaskAvailabilityManager.cpp.o: ../controller/TaskAvailabilityManager.h
CMakeFiles/plaster.dir/controller/TaskAvailabilityManager.cpp.o: ../controller/TaskAvailabilityResult.h
CMakeFiles/plaster.dir/controller/TaskAvailabilityManager.cpp.o: ../controller/TaskView.h
CMakeFiles/plaster.dir/controller/TaskAvailabilityManager.cpp.o: ../utils/Observable.h
CMakeFiles/plaster.dir/controller/TaskAvailabilityManager.cpp.o: ../utils/Observer.h

CMakeFiles/plaster.dir/controller/TaskAvailabilityResult.cpp.o: ../controller/TaskAvailabilityResult.cpp
CMakeFiles/plaster.dir/controller/TaskAvailabilityResult.cpp.o: ../controller/TaskAvailabilityResult.h
CMakeFiles/plaster.dir/controller/TaskAvailabilityResult.cpp.o: ../controller/TaskView.h

CMakeFiles/plaster.dir/controller/TaskLaunchOutcome.cpp.o: ../controller/TaskLaunchOutcome.cpp
CMakeFiles/plaster.dir/controller/TaskLaunchOutcome.cpp.o: ../controller/TaskLaunchOutcome.h
CMakeFiles/plaster.dir/controller/TaskLaunchOutcome.cpp.o: ../controller/TaskView.h

CMakeFiles/plaster.dir/controller/TaskLaunchOutcomeManager.cpp.o: ../controller/BinaryRepliesManager.h
CMakeFiles/plaster.dir/controller/TaskLaunchOutcomeManager.cpp.o: ../controller/TaskLaunchOutcome.h
CMakeFiles/plaster.dir/controller/TaskLaunchOutcomeManager.cpp.o: ../controller/TaskLaunchOutcomeManager.cpp
CMakeFiles/plaster.dir/controller/TaskLaunchOutcomeManager.cpp.o: ../controller/TaskLaunchOutcomeManager.h
CMakeFiles/plaster.dir/controller/TaskLaunchOutcomeManager.cpp.o: ../controller/TaskView.h
CMakeFiles/plaster.dir/controller/TaskLaunchOutcomeManager.cpp.o: ../utils/Observable.h
CMakeFiles/plaster.dir/controller/TaskLaunchOutcomeManager.cpp.o: ../utils/Observer.h

CMakeFiles/plaster.dir/controller/TaskLaunchRequest.cpp.o: ../controller/TaskLaunchRequest.cpp
CMakeFiles/plaster.dir/controller/TaskLaunchRequest.cpp.o: ../controller/TaskLaunchRequest.h

CMakeFiles/plaster.dir/controller/TaskView.cpp.o: ../controller/TaskView.cpp
CMakeFiles/plaster.dir/controller/TaskView.cpp.o: ../controller/TaskView.h

CMakeFiles/plaster.dir/events/AbortLaunchTask.cpp.o: ../controller/TaskView.h
CMakeFiles/plaster.dir/events/AbortLaunchTask.cpp.o: ../events/AbortLaunchTask.cpp
CMakeFiles/plaster.dir/events/AbortLaunchTask.cpp.o: ../events/AbortLaunchTask.h
CMakeFiles/plaster.dir/events/AbortLaunchTask.cpp.o: ../events/EventsVisitor.h
CMakeFiles/plaster.dir/events/AbortLaunchTask.cpp.o: ../events/PlasterEvent.h

CMakeFiles/plaster.dir/events/AskForAvailability.cpp.o: ../controller/TaskView.h
CMakeFiles/plaster.dir/events/AskForAvailability.cpp.o: ../events/AskForAvailability.cpp
CMakeFiles/plaster.dir/events/AskForAvailability.cpp.o: ../events/AskForAvailability.h
CMakeFiles/plaster.dir/events/AskForAvailability.cpp.o: ../events/EventsVisitor.h
CMakeFiles/plaster.dir/events/AskForAvailability.cpp.o: ../events/PlasterEvent.h

CMakeFiles/plaster.dir/events/CollectOutput.cpp.o: ../events/CollectOutput.cpp
CMakeFiles/plaster.dir/events/CollectOutput.cpp.o: ../events/CollectOutput.h
CMakeFiles/plaster.dir/events/CollectOutput.cpp.o: ../events/EventsVisitor.h
CMakeFiles/plaster.dir/events/CollectOutput.cpp.o: ../events/PlasterEvent.h

CMakeFiles/plaster.dir/events/FileChunkEvent.cpp.o: ../events/EventsVisitor.h
CMakeFiles/plaster.dir/events/FileChunkEvent.cpp.o: ../events/FileChunkEvent.cpp
CMakeFiles/plaster.dir/events/FileChunkEvent.cpp.o: ../events/FileChunkEvent.h
CMakeFiles/plaster.dir/events/FileChunkEvent.cpp.o: ../events/PlasterEvent.h

CMakeFiles/plaster.dir/events/FileRequestEvent.cpp.o: ../events/EventsVisitor.h
CMakeFiles/plaster.dir/events/FileRequestEvent.cpp.o: ../events/FileRequestEvent.cpp
CMakeFiles/plaster.dir/events/FileRequestEvent.cpp.o: ../events/FileRequestEvent.h
CMakeFiles/plaster.dir/events/FileRequestEvent.cpp.o: ../events/PlasterEvent.h

CMakeFiles/plaster.dir/events/KillApp.cpp.o: ../events/EventsVisitor.h
CMakeFiles/plaster.dir/events/KillApp.cpp.o: ../events/KillApp.cpp
CMakeFiles/plaster.dir/events/KillApp.cpp.o: ../events/KillApp.h
CMakeFiles/plaster.dir/events/KillApp.cpp.o: ../events/PlasterEvent.h

CMakeFiles/plaster.dir/events/LaunchCompleted.cpp.o: ../controller/TaskView.h
CMakeFiles/plaster.dir/events/LaunchCompleted.cpp.o: ../events/EventsVisitor.h
CMakeFiles/plaster.dir/events/LaunchCompleted.cpp.o: ../events/LaunchCompleted.cpp
CMakeFiles/plaster.dir/events/LaunchCompleted.cpp.o: ../events/LaunchCompleted.h
CMakeFiles/plaster.dir/events/LaunchCompleted.cpp.o: ../events/PlasterEvent.h

CMakeFiles/plaster.dir/events/LaunchTask.cpp.o: ../controller/Task.h
CMakeFiles/plaster.dir/events/LaunchTask.cpp.o: ../controller/TaskView.h
CMakeFiles/plaster.dir/events/LaunchTask.cpp.o: ../events/EventsVisitor.h
CMakeFiles/plaster.dir/events/LaunchTask.cpp.o: ../events/LaunchTask.cpp
CMakeFiles/plaster.dir/events/LaunchTask.cpp.o: ../events/LaunchTask.h
CMakeFiles/plaster.dir/events/LaunchTask.cpp.o: ../events/PlasterEvent.h

CMakeFiles/plaster.dir/events/NodeAvailability.cpp.o: ../controller/TaskView.h
CMakeFiles/plaster.dir/events/NodeAvailability.cpp.o: ../events/EventsVisitor.h
CMakeFiles/plaster.dir/events/NodeAvailability.cpp.o: ../events/NodeAvailability.cpp
CMakeFiles/plaster.dir/events/NodeAvailability.cpp.o: ../events/NodeAvailability.h
CMakeFiles/plaster.dir/events/NodeAvailability.cpp.o: ../events/PlasterEvent.h

CMakeFiles/plaster.dir/events/SendOutput.cpp.o: ../events/EventsVisitor.h
CMakeFiles/plaster.dir/events/SendOutput.cpp.o: ../events/PlasterEvent.h
CMakeFiles/plaster.dir/events/SendOutput.cpp.o: ../events/SendOutput.cpp
CMakeFiles/plaster.dir/events/SendOutput.cpp.o: ../events/SendOutput.h

CMakeFiles/plaster.dir/events/TaskCommunication.cpp.o: ../events/EventsVisitor.h
CMakeFiles/plaster.dir/events/TaskCommunication.cpp.o: ../events/PlasterEvent.h
CMakeFiles/plaster.dir/events/TaskCommunication.cpp.o: ../events/TaskCommunication.cpp
CMakeFiles/plaster.dir/events/TaskCommunication.cpp.o: ../events/TaskCommunication.h

CMakeFiles/plaster.dir/execution/TaskExecutor.cpp.o: ../api/TaskWrapper.h
CMakeFiles/plaster.dir/execution/TaskExecutor.cpp.o: ../controller/Task.h
CMakeFiles/plaster.dir/execution/TaskExecutor.cpp.o: ../controller/TaskView.h
CMakeFiles/plaster.dir/execution/TaskExecutor.cpp.o: ../events/PlasterEvent.h
CMakeFiles/plaster.dir/execution/TaskExecutor.cpp.o: ../events/TaskCommunication.h
CMakeFiles/plaster.dir/execution/TaskExecutor.cpp.o: ../execution/TaskExecutor.cpp
CMakeFiles/plaster.dir/execution/TaskExecutor.cpp.o: ../execution/TaskExecutor.h
CMakeFiles/plaster.dir/execution/TaskExecutor.cpp.o: ../network/NetworkManager.h
CMakeFiles/plaster.dir/execution/TaskExecutor.cpp.o: ../network/NodeIdMapper.h
CMakeFiles/plaster.dir/execution/TaskExecutor.cpp.o: ../network/RoutingTable.h
CMakeFiles/plaster.dir/execution/TaskExecutor.cpp.o: ../utils/Observable.h
CMakeFiles/plaster.dir/execution/TaskExecutor.cpp.o: ../utils/Observer.h

CMakeFiles/plaster.dir/network/NetworkManager.cpp.o: ../events/PlasterEvent.h
CMakeFiles/plaster.dir/network/NetworkManager.cpp.o: ../network/NetworkManager.cpp
CMakeFiles/plaster.dir/network/NetworkManager.cpp.o: ../network/NetworkManager.h
CMakeFiles/plaster.dir/network/NetworkManager.cpp.o: ../network/NodeIdMapper.h
CMakeFiles/plaster.dir/network/NetworkManager.cpp.o: ../network/RoutingTable.h
CMakeFiles/plaster.dir/network/NetworkManager.cpp.o: ../network/TCPClient.h
CMakeFiles/plaster.dir/network/NetworkManager.cpp.o: ../utils/Observable.h
CMakeFiles/plaster.dir/network/NetworkManager.cpp.o: ../utils/Observer.h

CMakeFiles/plaster.dir/network/NodeIdMapper.cpp.o: ../network/NodeIdMapper.cpp
CMakeFiles/plaster.dir/network/NodeIdMapper.cpp.o: ../network/NodeIdMapper.h

CMakeFiles/plaster.dir/network/RoutingTable.cpp.o: ../network/RoutingTable.cpp
CMakeFiles/plaster.dir/network/RoutingTable.cpp.o: ../network/RoutingTable.h

CMakeFiles/plaster.dir/network/TCPClient.cpp.o: ../network/TCPClient.cpp
CMakeFiles/plaster.dir/network/TCPClient.cpp.o: ../network/TCPClient.h

CMakeFiles/plaster.dir/network/TCPConnection.cpp.o: ../network/TCPConnection.cpp
CMakeFiles/plaster.dir/network/TCPConnection.cpp.o: ../network/TCPConnection.h

CMakeFiles/plaster.dir/network/TCPServer.cpp.o: ../network/TCPConnection.h
CMakeFiles/plaster.dir/network/TCPServer.cpp.o: ../network/TCPServer.cpp
CMakeFiles/plaster.dir/network/TCPServer.cpp.o: ../network/TCPServer.h

