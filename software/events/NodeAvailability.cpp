#include "NodeAvailability.h"
#include "EventsVisitor.h"

BOOST_CLASS_EXPORT_IMPLEMENT(NodeAvailability)

void NodeAvailability::accept(EventsVisitor &visitor) const {
    visitor.handle((NodeAvailability &) *this);
}

NodeAvailability::NodeAvailability(const std::string &sender, TaskView task)
        : PlasterEvent(sender), task(task) {

}

const TaskView &NodeAvailability::getTask() const {
    return task;
}

bool NodeAvailability::isNodeAvailable() const {
    return available;
}

void NodeAvailability::setIsNodeAvailable(const bool available) {
    this->available = available;
}
