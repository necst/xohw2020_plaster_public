#ifndef PLASTER_FILECHUNKEVENT_H
#define PLASTER_FILECHUNKEVENT_H

#include <string>
#include "PlasterEvent.h"
#include <boost/serialization/export.hpp>

class EventsVisitor;

/**
 * A specialized event used to represent packets containing chunks of an exchanged data
 */
class FileChunkEvent : public PlasterEvent {
public:

    static const int CHUNK_SIZE = 1024 * 1024 * 5; // 1 MB chunk

    FileChunkEvent() = default;

    void accept(EventsVisitor &visitor) const override;

    /**
     * Creates a new event containing chunks of data
     * @param sender the identifier of the node sending the data
     * @param identifier the identifier of the exchanged file
     * @param chunkData the payload of the chunk
     * @param isLast {@code True} if the packet is the last in the sequence, {@code false} otherwise
     */
    explicit FileChunkEvent(std::string &sender, const std::string &identifier, std::string &chunkData, bool isLast);

    const std::string &getChunkData() const;

    bool isLast() const;

    const std::string &getFileIdentifier() const;

private:
    friend class boost::serialization::access;

    std::string chunkData{};

    std::string fileIdentifier{};

    bool lastFlag{};

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & boost::serialization::base_object<PlasterEvent>(*this);
        ar & chunkData;
        ar & lastFlag;
        ar & fileIdentifier;
    }
};

BOOST_CLASS_EXPORT_KEY(FileChunkEvent)

#endif //PLASTER_FILECHUNKEVENT_H
