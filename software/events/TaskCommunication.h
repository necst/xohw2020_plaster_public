#ifndef PLASTER_TASKCOMMUNICATION_H
#define PLASTER_TASKCOMMUNICATION_H


#include "PlasterEvent.h"
#include <boost/serialization/export.hpp>

class EventsVisitor;

/**
 * A specialized network event representing a message exchanged between tasks, containing information about:
 * - the node that sent the message
 * - the type of message
 * - the app/task combination the message comes from
 * - the actual content of the message, to be expressed as a JSON string
 */
class TaskCommunication : public PlasterEvent {

public:

    TaskCommunication() = default;

    explicit TaskCommunication(const std::string &sender, const std::string &messageType,
                               const std::string &messagePayload);

    ~TaskCommunication() = default;

    void accept(EventsVisitor &visitor) const override;

    const std::string &getMessageType() const;

    const std::string &getMessagePayload() const;

    const std::string &getTaskName() const;

    void setTaskName(const std::string &taskName);

    const std::string &getAppName() const;

    void setAppName(const std::string &appName);

private:
    friend class boost::serialization::access;

    std::string taskName{};
    std::string appName{};
    std::string messageType{};
    std::string messagePayload{};


    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & boost::serialization::base_object<PlasterEvent>(*this);
        ar & messageType;
        ar & messagePayload;
        ar & appName;
        ar & taskName;
    }

};

BOOST_CLASS_EXPORT_KEY(TaskCommunication)

#endif //PLASTER_TASKCOMMUNICATION_H
