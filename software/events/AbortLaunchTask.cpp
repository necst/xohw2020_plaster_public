
#include "AbortLaunchTask.h"
#include "EventsVisitor.h"
#include "../controller/TaskView.h"

BOOST_CLASS_EXPORT_IMPLEMENT(AbortLaunchTask)

void AbortLaunchTask::accept(EventsVisitor &visitor) const
{
    visitor.handle((AbortLaunchTask &)*this);
}

AbortLaunchTask::AbortLaunchTask(const std::string &sender, TaskView task) : PlasterEvent(sender), task(task) {}

const TaskView AbortLaunchTask::getTask() const
{
    return task;
}
