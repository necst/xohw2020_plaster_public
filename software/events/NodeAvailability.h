#ifndef PLASTER_NODEAVAILABILITY_H
#define PLASTER_NODEAVAILABILITY_H

#include "PlasterEvent.h"
#include "../controller/TaskView.h"
#include <boost/serialization/export.hpp>

class EventsVisitor;

class NodeAvailability : public PlasterEvent {
public:

    NodeAvailability() = default;

    void accept(EventsVisitor &visitor) const override;

    /**
     * Creates a new instance of the event, holding information about the sender and the task
     * @param sender the identifier of the node that sent the event, not empty
     * @param task the object representing the task's information
     */
    NodeAvailability(const std::string &sender, TaskView task);

    /**
     * Retrieves whether the node is available to execute the task
     * @return {@code true} if the node is available, {@code false} otherwise
     */
    bool isNodeAvailable() const;

    /**
     * Retrieves the task the availability is referred to
     * @return the object representing the task's information
     */
    const TaskView &getTask() const;

    /**
     * Sets whether the node is available to execute the task
     * @param available {@code true} if the node is available, {@code false} otherwise
     */
    void setIsNodeAvailable(bool available);

private:
    friend class boost::serialization::access;

    TaskView task{};
    bool available = false;

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & boost::serialization::base_object<PlasterEvent>(*this);
        ar & task;
        ar & available;
    }
};

BOOST_CLASS_EXPORT_KEY(NodeAvailability)

#endif
