#include "AskForAvailability.h"
#include "EventsVisitor.h"

BOOST_CLASS_EXPORT_IMPLEMENT(AskForAvailability)

void AskForAvailability::accept(EventsVisitor &visitor) const {
    visitor.handle((AskForAvailability &) *this);
    // TODO: When the event is handled by the visitor, it should reserve the required resources (if available) and reply
}

AskForAvailability::AskForAvailability(const std::string &senderId, TaskView task, double requiredCpu,
                                       int requiredFpga) : task(task), requiredCpu(requiredCpu),
                                                           requiredFpga(requiredFpga),
                                                           PlasterEvent(senderId) {
    if (senderId.empty()) {
        throw std::invalid_argument("Sender id cannot be empty");
    }
    if (requiredCpu < 0 || requiredFpga < 0) {
        throw std::invalid_argument("Found negative parameters");
    }
}

const TaskView &AskForAvailability::getTask() const {
    return task;
}

double AskForAvailability::getRequiredCpu() const {
    return requiredCpu;
}

int AskForAvailability::getRequiredFpga() const {
    return requiredFpga;
}
