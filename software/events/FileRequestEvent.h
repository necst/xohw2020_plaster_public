#ifndef PLASTER_FILEREQUESTEVENT_H
#define PLASTER_FILEREQUESTEVENT_H

#include <string>
#include "PlasterEvent.h"
#include <boost/serialization/export.hpp>

class EventsVisitor;

class FileRequestEvent : public PlasterEvent {
public:
    FileRequestEvent() = default;

    void accept(EventsVisitor &visitor) const override;

    explicit FileRequestEvent(std::string &sender, std::string &filePath, const std::string &fileIdentifier);

    const std::string &getFilePath() const;

    const std::string &getFileIdentifier() const;

private:
    friend class boost::serialization::access;

    std::string filePath{};

    std::string fileIdentifier{};

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & boost::serialization::base_object<PlasterEvent>(*this);
        ar & filePath;
        ar & fileIdentifier;
    }

};

BOOST_CLASS_EXPORT_KEY(FileRequestEvent)

#endif //PLASTER_FILEREQUESTEVENT_H
