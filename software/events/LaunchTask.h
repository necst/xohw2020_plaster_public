#ifndef PLASTER_LAUNCHTASK_H
#define PLASTER_LAUNCHTASK_H

#include "PlasterEvent.h"
#include "../controller/Task.h"
#include "../controller/TaskView.h"
#include <boost/serialization/export.hpp>

/**
 * A specialized event that is sent to a node when it is selected to execute a task (given that he reported availability).
 * Upon receiving this event, the node should launch and start executing the task
 */
class EventsVisitor;

class LaunchTask : public PlasterEvent {
public:

    LaunchTask() = default;

    /**
     * Creates a new instance of the event, asking to launch the provided task
     * @param sender the identifier of the node that sent the event, not empty
     * @param task the object representing the task's information
     */
    explicit LaunchTask(const std::string &sender, TaskView task);

    /**
     * Retrieves the taks that has to be launched
     * @return the object representing the task's information
     */
    const TaskView &getTask() const;

    void accept(EventsVisitor &visitor) const override;


private:
    friend class boost::serialization::access;

    TaskView task{};

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & boost::serialization::base_object<PlasterEvent>(*this);
        ar & task;
    }
};

BOOST_CLASS_EXPORT_KEY(LaunchTask)

#endif
