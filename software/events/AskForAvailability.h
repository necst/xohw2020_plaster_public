#ifndef PLASTER_ASKFORAVAILABILITY_H
#define PLASTER_ASKFORAVAILABILITY_H

#include "PlasterEvent.h"
#include "../controller/TaskView.h"
#include <boost/serialization/export.hpp>

class EventsVisitor;

/**
 * A specialized event that is sent whenever a new task has to be launched and its allocation has to be determined
 */
class AskForAvailability : public PlasterEvent {
public:

    AskForAvailability() = default;

    /**
     * Creates a new instance, asking availability of the provided amount of CPU and FPGA
     * @param senderId the identifier of the node that sent the event, not empty
     * @param task the object representing the task's information
     * @param requiredCpu the required amount of CPUs for the task, not negative
     * @param requiredFpga the required amount of FPGAs for the task, not negative
     */
    explicit AskForAvailability(const std::string &senderId, TaskView task, double requiredCpu, int requiredFpga);

    /**
     * Retrieves the task associated to the request
     * @return the object representing the task's information
    **/
    const TaskView &getTask() const;

    /**
     * Get the amount of CPU that has been requested
     * @return the requested amount of CPU
     */
    double getRequiredCpu() const;

    /**
     * Get the amount of FPGA that has been requested
     * @return the requested amount of FPGA
     */
    int getRequiredFpga() const;

    void accept(EventsVisitor &visitor) const override;

private:
    TaskView task{};
    double requiredCpu{};
    int requiredFpga{};

    friend class boost::serialization::access;

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & boost::serialization::base_object<PlasterEvent>(*this);
        ar & task;
        ar & requiredCpu;
        ar & requiredFpga;
    }
};

BOOST_CLASS_EXPORT_KEY(AskForAvailability)

#endif //PLASTER_ASKFORAVAILABILITY_H