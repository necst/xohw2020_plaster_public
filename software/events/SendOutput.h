#ifndef PLASTER_SENDOUTPUT_H
#define PLASTER_SENDOUTPUT_H

#include "PlasterEvent.h"

class EventsVisitor;

class SendOutput : public PlasterEvent {
public:
    SendOutput() = default;

    void accept(EventsVisitor &visitor) const override;

private:
    friend class boost::serialization::access;

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & boost::serialization::base_object<PlasterEvent>(*this);
    }
};

BOOST_CLASS_EXPORT_KEY(SendOutput)

#endif
