#include "TaskCommunication.h"
#include "EventsVisitor.h"

BOOST_CLASS_EXPORT_IMPLEMENT(TaskCommunication)

void TaskCommunication::accept(EventsVisitor &visitor) const {
    visitor.handle((TaskCommunication &) *this);
}

TaskCommunication::TaskCommunication(const std::string &sender, const std::string &messageType,
                                     const std::string &messagePayload) : PlasterEvent(sender),
                                                                          messageType(messageType),
                                                                          messagePayload(messagePayload) {}

const std::string &TaskCommunication::getTaskName() const {
    return taskName;
}

void TaskCommunication::setTaskName(const std::string &taskName) {
    TaskCommunication::taskName = taskName;
}

const std::string &TaskCommunication::getAppName() const {
    return appName;
}

void TaskCommunication::setAppName(const std::string &appName) {
    TaskCommunication::appName = appName;
}

const std::string &TaskCommunication::getMessageType() const {
    return messageType;
}

const std::string &TaskCommunication::getMessagePayload() const {
    return messagePayload;
}
