
#ifndef PLASTER_EVENTSVISITOR_H
#define PLASTER_EVENTSVISITOR_H

// Forward declarations of events
class AbortLaunchTask;

class KillApp;

class AskForAvailability;

class CollectOutput;

class LaunchCompleted;

class LaunchTask;

class NodeAvailability;

class SendOutput;

class FileRequestEvent;

class FileChunkEvent;

class TaskCommunication;

class EventsVisitor {

public:
    virtual void handle(AbortLaunchTask &) = 0;

    virtual void handle(KillApp &) = 0;

    virtual void handle(AskForAvailability &) = 0;

    virtual void handle(CollectOutput &) = 0;

    virtual void handle(LaunchCompleted &) = 0;

    virtual void handle(LaunchTask &) = 0;

    virtual void handle(NodeAvailability &) = 0;

    virtual void handle(SendOutput &) = 0;

    virtual void handle(FileRequestEvent &) = 0;

    virtual void handle(FileChunkEvent &) = 0;

    virtual void handle(TaskCommunication &) = 0;
};


#endif //PLASTER_EVENTSVISITOR_H
