#include "FileChunkEvent.h"
#include "EventsVisitor.h"

BOOST_CLASS_EXPORT_IMPLEMENT(FileChunkEvent)

const std::string &FileChunkEvent::getChunkData() const {
    return chunkData;
}

void FileChunkEvent::accept(EventsVisitor &visitor) const {
    visitor.handle((FileChunkEvent &) *this);
}

FileChunkEvent::FileChunkEvent(std::string &sender, const std::string &identifier, std::string &chunkData, bool isLast)
        :
        PlasterEvent(sender), chunkData(chunkData), lastFlag(isLast), fileIdentifier(identifier) {
}

bool FileChunkEvent::isLast() const {
    return lastFlag;
}

const std::string &FileChunkEvent::getFileIdentifier() const {
    return fileIdentifier;
}
