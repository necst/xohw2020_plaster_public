#include "LaunchTask.h"
#include "EventsVisitor.h"

BOOST_CLASS_EXPORT_IMPLEMENT(LaunchTask)

void LaunchTask::accept(EventsVisitor &visitor) const {
    visitor.handle((LaunchTask &) *this);
}

LaunchTask::LaunchTask(const std::string &sender, TaskView task) : task(task), PlasterEvent(sender) {}

const TaskView &LaunchTask::getTask() const {
    return task;
}
