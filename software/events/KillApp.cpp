
#include "KillApp.h"
#include "EventsVisitor.h"

BOOST_CLASS_EXPORT_IMPLEMENT(KillApp)

void KillApp::accept(EventsVisitor &visitor) const {
    visitor.handle((KillApp &) *this);
}

KillApp::KillApp(const std::string &senderId, const std::string &appId) : appId(appId), PlasterEvent(senderId) {
    if (senderId.empty() || appId.empty()) {
        throw std::invalid_argument("Identifiers cannot be empty");
    }
}

const std::string &KillApp::getAppId() const {
    return appId;
}
