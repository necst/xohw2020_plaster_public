#ifndef PLASTER_KILLAPP_H
#define PLASTER_KILLAPP_H

#include "PlasterEvent.h"
#include <boost/serialization/export.hpp>

/**
 * A specialized event used to stop the execution of an app.
 * It should be handled by all the nodes containing its task,
 * in order to stop them and free the resources used by them.
 */
class KillApp : public PlasterEvent {
public:

    KillApp() = default;

    void accept(EventsVisitor &visitor) const override;

    /**
     * Creates an instance of the event to kill the provided app
     * @param senderId the identifier of the node that sent the event, not empty
     * @param appId the identifier of the app to kill, not empty
     */
    explicit KillApp(const std::string &senderId, const std::string &appId);

    /**
     * Retrieves the identifier of the app to kill
     * @return a string containing the identifier of the app
     */
    const std::string &getAppId() const;

private:
    friend class boost::serialization::access;

    std::string appId;

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & boost::serialization::base_object<PlasterEvent>(*this);
        ar & appId;
    }
};

BOOST_CLASS_EXPORT_KEY(KillApp)

#endif
