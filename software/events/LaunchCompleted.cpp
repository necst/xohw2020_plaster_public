#include "LaunchCompleted.h"
#include "EventsVisitor.h"

BOOST_CLASS_EXPORT_IMPLEMENT(LaunchCompleted)

void LaunchCompleted::accept(EventsVisitor &visitor) const {
    visitor.handle((LaunchCompleted &) *this);
}

LaunchCompleted::LaunchCompleted(const std::string &sender, TaskView task, bool launchSuccessful) :
        PlasterEvent(sender),
        task(task),
        launchSuccessful(launchSuccessful) {
}

const TaskView &LaunchCompleted::getTask() const {
    return task;
}

bool LaunchCompleted::isLaunchSuccessful() const {
    return launchSuccessful;
}
