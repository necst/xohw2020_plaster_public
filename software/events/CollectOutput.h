#ifndef PLASTER_COLLECTOUTPUT_H
#define PLASTER_COLLECTOUTPUT_H

#include "PlasterEvent.h"
#include <boost/serialization/export.hpp>

class EventsVisitor;

class CollectOutput : public PlasterEvent {
public:

    CollectOutput() = default;

    void accept(EventsVisitor &visitor) const override;

private:
    friend class boost::serialization::access;

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & boost::serialization::base_object<PlasterEvent>(*this);
    }
};

BOOST_CLASS_EXPORT_KEY(CollectOutput)

#endif