#include "CollectOutput.h"
#include "EventsVisitor.h"

BOOST_CLASS_EXPORT_IMPLEMENT(CollectOutput)

void CollectOutput::accept(EventsVisitor &visitor) const {
    visitor.handle((CollectOutput &) *this);
}
