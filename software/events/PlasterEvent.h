#ifndef PLASTER_PLASTEREVENT_H
#define PLASTER_PLASTEREVENT_H

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/assume_abstract.hpp>
#include <iostream>
#include <sstream>

using namespace boost::archive;

// Forward declaration of visitor
class EventsVisitor;

/**
 * An abstract class describing an event that is triggered by a message
 * received by a node in the network.
 * It also implements the visitor pattern and is able to accept a visitor
 * to be handled accordingly
 */
class PlasterEvent {
private:
    friend class boost::serialization::access;

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & sender_;
    };
    /**
     * The id of the node that sent the event
     */
    std::string sender_;

public:
    PlasterEvent() = default;

    /**
     * Creates a new instance of the event, sent by the given node
     * @param sender A string containing the identifier of the node, not empty
     */
    explicit PlasterEvent(const std::string &sender) : sender_(sender) {
        if (sender.empty()) {
            throw std::invalid_argument("Sender id cannot be null");
        }
    }

    virtual void accept(EventsVisitor &visitor) const = 0;

    /**
     * Retrieves the node that sent the event
     * @return A string containing the identifier of the node
     */
    const std::string &getSender() const {
        return sender_;
    }
};

BOOST_SERIALIZATION_ASSUME_ABSTRACT(PlasterEvent)

#endif //PLASTER_PLASTEREVENT_H