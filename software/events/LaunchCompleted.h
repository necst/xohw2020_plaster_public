#ifndef PLASTER_LAUNCHCOMPLETED_H
#define PLASTER_LAUNCHCOMPLETED_H

#include "PlasterEvent.h"
#include "../controller/TaskView.h"
#include <boost/serialization/export.hpp>

/**
 * A specialized event that is sent as the last step of a task launch to notify its completion.
 * It is fired by the nodes that launched the corresponding task and handled by the node manager
 * that initiated its startup
 */
class EventsVisitor;

class LaunchCompleted : public PlasterEvent {

public:

    LaunchCompleted() = default;

    void accept(EventsVisitor &visitor) const override;

    /**
     * Creates a new instance of the event, sent by the provided node and containing the outcome of the launch for the given task
     * @param sender The identifier of the node, not empty
     * @param task the object representing the task's information
     * @param launchSuccessful A boolean value containing the outcome of the launch
     */
    explicit LaunchCompleted(const std::string &sender, TaskView task, bool launchSuccessful);

    const TaskView &getTask() const;

    bool isLaunchSuccessful() const;

private:
    friend class boost::serialization::access;

    TaskView task{};

    /**
     * A boolean value indicating whether the launch was successful or not
     */
    bool launchSuccessful{};

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & boost::serialization::base_object<PlasterEvent>(*this);
        ar & task;
        ar & launchSuccessful;
    }
};

BOOST_CLASS_EXPORT_KEY(LaunchCompleted)

#endif
