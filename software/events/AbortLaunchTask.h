#ifndef PLASTER_ABORTLAUNCHTASK_H
#define PLASTER_ABORTLAUNCHTASK_H

#include "PlasterEvent.h"
#include "../controller/TaskView.h"
#include <boost/serialization/export.hpp>

class EventsVisitor;

/**
 * A specialized event used to abort the launch of a task holding information about the task to abort.
 * It will be fired by the node manager that is handling the launch of a task whenever the received
 * replies to the availability query are not sufficient to start executing the task.
 */
class AbortLaunchTask : public PlasterEvent {
public:

    AbortLaunchTask() = default;

    void accept(EventsVisitor &visitor) const override;

    /**
     * Creates a new instance of the event to abort the launch of the specified task
     * @param sender The name of the node that sent the event
     * @param task The object representing the task's information
     */
    explicit AbortLaunchTask(const std::string &sender, TaskView task);

    /**
     * Retrieves the associated task
     * @return The object representing the task's information
     */
    const TaskView getTask() const;

private:
    friend class boost::serialization::access;

    TaskView task{};

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & boost::serialization::base_object<PlasterEvent>(*this);
        ar & task;
    }
};

BOOST_CLASS_EXPORT_KEY(AbortLaunchTask)

#endif //PLASTER_ABORTLAUNCHTASK_H
