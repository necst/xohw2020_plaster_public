#include "FileRequestEvent.h"
#include "EventsVisitor.h"

BOOST_CLASS_EXPORT_IMPLEMENT(FileRequestEvent)

const std::string &FileRequestEvent::getFilePath() const {
    return filePath;
}

void FileRequestEvent::accept(EventsVisitor &visitor) const {
    visitor.handle((FileRequestEvent &) *this);
}

FileRequestEvent::FileRequestEvent(std::string &sender, std::string &filePath, const std::string &fileIdentifier) :
        PlasterEvent(sender), filePath(filePath), fileIdentifier(fileIdentifier) {
}

const std::string &FileRequestEvent::getFileIdentifier() const {
    return fileIdentifier;
}
