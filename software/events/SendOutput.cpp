#include "SendOutput.h"
#include "EventsVisitor.h"

BOOST_CLASS_EXPORT_IMPLEMENT(SendOutput)

void SendOutput::accept(EventsVisitor &visitor) const {
    visitor.handle((SendOutput &) *this);
}
