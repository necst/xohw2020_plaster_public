#ifndef PLASTER_ROUTINGTABLE_H
#define PLASTER_ROUTINGTABLE_H

#include <map>

using namespace std;

/**
 * A class describing a table used to store information about the IP addresses of the nodes present in the cluster.
 * It provides methods to store, retrieve and deleted the collected information.
 */
class RoutingTable {
private:
    map<string, string> lookup;
public:

    RoutingTable();

    /**
     * Get the IP address associated to the provided node
     * @param nodeId The ID of the node to retrieve
     * @return The string containing the IP address of the node, empty if not known
     */
    string getIpAddress(const string &nodeId);

    /**
     * Retrieves the number of active peers discovered in the network
     * @return the number of active peers in the network
     */
    int getPeersCount();

    /**
     * Store the provided IP address for the provided node,
     * if the node is already present it will just update its address
     * @param nodeId  The ID of the node
     * @param ipAddress The IP address of the node
     */
    void addEntry(const string &nodeId, const string &ipAddress);

    /**
     * Delete the information associated to the provided node
     * @param nodeId The ID of the node to remove
     */
    void removeEntry(const string &nodeId);
};


#endif //PLASTER_ROUTINGTABLE_H
