#ifndef PLASTER_NODEIDMAPPER_H
#define PLASTER_NODEIDMAPPER_H

#include <string>
#include <map>


/**
 * A class representing an object that holds information about the network identifier
 * of each peer and its corresponding friendly name
 */
class NodeIdMapper {

private:
    std::map<std::string, std::string> identifiers{};
public:
    /**
     * Associates the provided friendly name to the network identifier of the peer
     * @param friendlyName the friendly name of the peer, not empty
     * @param peerId the network identifier of the peer, not empty
     */
    void addEntry(const std::string &friendlyName, const std::string &peerId);

    /**
     * Retrieves the network identifier of a peer having the provided friendly name,
     * @return a string containing the network identifier, empty if no association is present
     */
    std::string getPeerId(const std::string &friendlyName) const;

    //TODO: Check whether to add a method to remove association

};


#endif //PLASTER_NODEIDMAPPER_H
