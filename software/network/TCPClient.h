#ifndef PLASTER_TCPCLIENT_H
#define PLASTER_TCPCLIENT_H

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

#include <iostream>
#include <fstream>
#include <boost/enable_shared_from_this.hpp>

#define BUFF_SIZE 1024

using boost::asio::ip::tcp;

/**
 * A class used to represent a client used for a TCP file transfer, with internal methods to
 * handle the connection and write the incoming files to disk
 */
class TCPClient {

public:
    TCPClient(boost::asio::io_service &io_service, const std::string &server, const std::string &path);

private:

    void handle_resolve(const boost::system::error_code &err, tcp::resolver::iterator endpoint_iterator);

    void handle_connect(const boost::system::error_code &err, tcp::resolver::iterator endpoint_iterator);

    void handle_write_file(const boost::system::error_code &err);

    tcp::resolver resolver_;
    tcp::socket socket_;
    boost::array<char, BUFF_SIZE> buf;
    boost::asio::streambuf request_;
    std::ifstream source_file;
};

#endif //PLASTER_TCPCLIENT_H
