#include "RoutingTable.h"

RoutingTable::RoutingTable() {
    this->lookup = map<string, string>();
}

/**
 * Get the IP address associated to the provided node
 * @param nodeId The ID of the node to retrieve
 * @return The string containing the IP address of the node, empty if not known
 */
string RoutingTable::getIpAddress(const string &nodeId) {
    // First try to get the address of the node
    try {
        return lookup.at(nodeId);
    }
    catch (out_of_range &e) {
        printf("[ROUTING] provided node ID is not present");
        // If the node ID is not present, return an empty string
        return string();
    }
}

/**
 * Store the provided IP address for the provided node,
 * if the node is already present it will just update its address
 * @param nodeId  The ID of the node
 * @param ipAddress The IP address of the node
 */
void RoutingTable::addEntry(const string &nodeId, const string &ipAddress) {
    // First check whether the strings are non-empty
    if (nodeId.empty() || ipAddress.empty()) {
        throw std::invalid_argument("Provided entries cannot be empty");
    }
    // Insert the provided strings in the map
    lookup[nodeId] = ipAddress;
}

/**
 * Delete the information associated to the provided node
 * @param nodeId The ID of the node to remove
 */
void RoutingTable::removeEntry(const string &nodeId) {
    // First check whether the ID is empty
    if (nodeId.empty()) {
        throw std::invalid_argument("Node id cannot be empty");
    }
    // Then remove its entry from the table
    lookup.erase(nodeId);
}

int RoutingTable::getPeersCount() {
    return lookup.size();
}
