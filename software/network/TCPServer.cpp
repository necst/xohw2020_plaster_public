#include "TCPServer.h"

TCPServer::TCPServer(unsigned short port) : acceptor_(io_service_,
                                                      boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port),
                                                      true) {
    ptr_async_tcp_connection new_connection_(new TCPConnection(io_service_));
    acceptor_.async_accept(new_connection_->socket(),
                           boost::bind(&TCPServer::handle_accept, this, new_connection_,
                                       boost::asio::placeholders::error));
    io_service_.run();
}

TCPServer::~TCPServer() {
    io_service_.stop();
}

void
TCPServer::handle_accept(TCPServer::ptr_async_tcp_connection current_connection, const boost::system::error_code &e) {
    if (!e) {
        current_connection->start();
    }
}
