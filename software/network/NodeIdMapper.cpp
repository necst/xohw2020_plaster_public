#include "NodeIdMapper.h"

void NodeIdMapper::addEntry(const std::string &friendlyName, const std::string &peerId) {
    if (friendlyName.empty() || peerId.empty()) {
        throw std::invalid_argument("Found empty arguments");
    }
    identifiers[friendlyName] = peerId;
}

std::string NodeIdMapper::getPeerId(const std::string &friendlyName) const {
    try {
        return this->identifiers.at(friendlyName);
    }
    catch (std::out_of_range &e) {
        printf("[ID_MAPPER] missing association for %s", friendlyName.c_str());
        return std::string();
    }
}
