#include "NetworkManager.h"
#include "TCPClient.h"
#include <zyre.h>
#include <zyre_library.h>
#include <thread>
#include <iostream>
#include <boost/asio/io_service.hpp>

#define GROUP "PLASTER"

/**
 * Retrieve the IP address from the provided address of the node
 * @param rawAddress the raw address of the node
 * @return the cleaned ip address of the node
 */
static std::string parseIpAddress(const std::string &rawAddress) {
    return rawAddress.substr(6, rawAddress.size() - 12);
}


/**
 * Create a new NetworkManager with the provided name
 * @param name The string containing the name of the node, not empty
 */
NetworkManager::NetworkManager(const std::string &name, const std::string &networkInterface) :
        isRunning(true),
        routingTable(),
        eventsBuffer() {
    // Check whether the provided name is valid
    if (name.empty()) {
        printf("[NETWORK] Cannot create a network manager with an empty name\n");
        // TODO: See how to notify error
        return;
    }
    // Set the name of the node
    printf("[NETWORK] Creating Network manager with name %s\n", name.c_str());
    this->name = name;

    // Then set up the zyre node and create a new zyre node
    this->node = zyre_new(name.c_str());
    assert(node);
    // If a network interface has been provided, use it
    if(!networkInterface.empty()){
        zyre_set_interface(node, networkInterface.c_str());
    }
    zyre_start(node);
    // And make it join the group of the cluster
    int joinResult = zyre_join(node, GROUP);
    cout << "[NETWORK_MANAGER] JOIN RESULT: " << joinResult << std::endl;
    zyre_dump(node);
    // Create the listener and dispatcher threads
    this->listenerTask = std::thread(&NetworkManager::listener, this, node);
    this->dispatcherTask = std::thread(&NetworkManager::dispatcher, this, node);
}

NetworkManager::~NetworkManager() {
    this->stop();
}

/**
 * Stop the node from listening to messages in the network
 */
void NetworkManager::stop() {
    printf("[NETWORK] Stopping thread\n");
    // Change the running flag
    this->isRunning = false;
    // First check whether the thread can be joined (or has already stopped)
    if (!this->listenerTask.joinable()) {
        return;
    }
    // And join the thread
    listenerTask.join();
    if (!this->dispatcherTask.joinable()) {
        return;
    }
    dispatcherTask.join();
    if (!node) {
        printf("[NETWORK] No connection to close\n");
        return;
    }
    printf("[NETWORK] Closing connection...\n");
    // First stop the node and disconnect from the group
    zyre_stop(node);
    // Then destroy the object
    zyre_destroy(&node);
}

/**
 * The main function executed in the thread
 * @param znode The object representing the zyre node
 */
void NetworkManager::listener(zyre_t *znode) {
    assert(znode);
    // Main loop
    while (this->isRunning) {
        // Listen for a message
        zmsg_t *msg = zyre_recv(znode);
        // Once a message is receive, spawn a new node to handle it
        std::thread(&NetworkManager::handleMessage, this, msg).detach();
        // And sleep for a little while
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

/**
 * Continuously check for messages to be sent and publish them on the network
 * @param znode The object representing the zyre node
 */
void NetworkManager::dispatcher(zyre_t *znode) {
    assert(znode);
    while (this->isRunning) {
        // Check whether there are messages waiting
        // TODO: Check whether to wait for the queue not to be empty
        if (!eventsBuffer.empty()) {
            // Serialize the object:
            // First create a stream
            std::stringstream ss;
            // Then create an archive to wrap the stream
            text_oarchive oa{ss};
            // Get the first element in the buffer
            PlasterEvent *event = eventsBuffer.front().first;
            // And serialize it
            oa << event;
            // Then retrieve all the recipients
            std::list<std::string> recipients = eventsBuffer.front().second;
            if (recipients.empty()) {
                // Shout the event as broadcast
                zyre_shouts(znode, GROUP, "%s", ss.str().c_str());
                // And make the node handle it
                auto message = reinterpret_cast<PlasterEvent *>(event);
                std::thread(&NetworkManager::selfHandleMessage, this, message).detach();
            } else {
                // Send the message just to the specified nodes using WHISPER mode
                for (auto &recipient : recipients) {
                    if (strcmp(recipient.c_str(), name.c_str()) == 0) {
                        auto message = reinterpret_cast<PlasterEvent *>(event);
                        std::thread(&NetworkManager::selfHandleMessage, this, message).detach();
                    } else {
                        std::string peerId = nodeIdMapper.getPeerId(recipient);
                        zyre_whispers(znode, peerId.c_str(), "%s", ss.str().c_str());
                    }
                }
            }
            // Remove it from the queue
            eventsBuffer.pop();
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}

/* ===== INTERNAL METHODS ===== */

/**
 * Handle an incoming message from the network
 * @param msg The object representing the message
 */
void NetworkManager::handleMessage(zmsg_t *msg) {
    // Get the event type of the message
    std::string event = zmsg_popstr(msg);
    // The string containing the id of the peer that sent the message
    char *peerName = zmsg_popstr(msg);
    // The string containing the content of the message
    char *message = zmsg_popstr(msg);
    // The string containing the name of the group the sender belongs to
    char *group = zmsg_popstr(msg);
    // The string containing the actual content
    char *content = zmsg_popstr(msg);
    // And then update the routing table according to it
    if (event == "ENTER") {
        std::cout << "Entered new node: " << peerName << " with ID as: " << message << std::endl;
        routingTable.addEntry(message, parseIpAddress(content));
        nodeIdMapper.addEntry(message, peerName);
    } else if (event == "LEAVE") {
        routingTable.removeEntry(peerName);
    } else if (event == "SHOUT" || event == "WHISPER") {
        std::stringstream ss(
                event == "SHOUT" ? content : group
        );
        text_iarchive ia{ss};
        PlasterEvent *receivedEvent;
        ia >> receivedEvent;
        // DO NOT CONSIDER EVASIVE OR SILENT EVENTS
        zmsg_print(msg);
        notifyObservers(*receivedEvent);
    }
}

void NetworkManager::broadcastEvent(PlasterEvent *event) {
    // Send the event as broadcast, by passing an empty list to the overloaded method
    sendEvent(event, std::list<std::string>{});
}

void NetworkManager::sendEvent(PlasterEvent *event, std::list<std::string> recipients) {
    eventsBuffer.push(std::pair<PlasterEvent *, std::list<std::string>>{event, recipients});
}

int NetworkManager::getPeersCount() {
    return routingTable.getPeersCount() + 1;
}

void NetworkManager::sendFile(const string &filePath, const string &nodeId) {
    try {
        boost::asio::io_service io_service;
        std::string clientAddress = routingTable.getIpAddress(nodeId) + ":6767";
        TCPClient client(io_service, clientAddress, filePath);
        io_service.run();
    }
    catch (std::exception const &e) {
        std::cerr << e.what() << std::endl;
    }
}

void NetworkManager::selfHandleMessage(PlasterEvent *event) {
    notifyObservers(*event);
}
