#ifndef PLASTER_TCPSERVER_H
#define PLASTER_TCPSERVER_H

#include <boost/core/noncopyable.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include "TCPConnection.h"

class TCPServer : private boost::noncopyable {
public:
    typedef boost::shared_ptr<TCPConnection> ptr_async_tcp_connection;

    TCPServer(unsigned short port);

    ~TCPServer();

    void handle_accept(ptr_async_tcp_connection current_connection, const boost::system::error_code &e);

private:
    boost::asio::io_service io_service_;
    boost::asio::ip::tcp::acceptor acceptor_;
};


#endif //PLASTER_TCPSERVER_H
