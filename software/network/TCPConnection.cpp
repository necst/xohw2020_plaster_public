#include <iostream>
#include "TCPConnection.h"

void TCPConnection::handle_read_request(const boost::system::error_code &err, std::size_t bytes_transferred) {
    if (err) {
        std::cout << "Error";
    }
    std::istream request_stream(&request_buf);
    std::string file_path;
    request_stream >> file_path;
    request_stream >> file_size;
    request_stream.read(buf.c_array(), 2);

    size_t pos = file_path.find_last_of('\\');
    if (pos != std::string::npos) {
        file_path = file_path.substr(pos + 1);
    }
    output_file.open(file_path + ".received", std::ios_base::binary);
    if (!output_file) {
        return;
    }
    do {
        request_stream.read(buf.c_array(), (std::streamsize) buf.size());
        output_file.write(buf.c_array(), request_stream.gcount());
    } while (request_stream.gcount() > 0);
    async_read(socket_, boost::asio::buffer(buf.c_array(), buf.size()),
               boost::bind(&TCPConnection::handle_read_file_content, shared_from_this(),
                           boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void TCPConnection::handle_read_file_content(const boost::system::error_code &err, std::size_t bytes_transferred) {
    if (bytes_transferred > 0) {
        output_file.write(buf.c_array(), (std::streamsize) bytes_transferred);
        if (output_file.tellp() >= (std::streamsize) file_size) {
            return;
        }
    }
    if (err) {
        std::cout << "Error";
    }
    async_read(socket_, boost::asio::buffer(buf.c_array(), buf.size()),
               boost::bind(&TCPConnection::handle_read_file_content, shared_from_this(),
                           boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void TCPConnection::start() {
    async_read_until(socket_, request_buf, "\n\n",
                     boost::bind(&TCPConnection::handle_read_request, shared_from_this(),
                                 boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

boost::asio::ip::tcp::socket &TCPConnection::socket() {
    return socket_;
}
