#ifndef PLASTER_NETWORKMANAGER_H
#define PLASTER_NETWORKMANAGER_H

#include "../utils/Observable.h"
#include "RoutingTable.h"
#include "NodeIdMapper.h"
#include "../events/PlasterEvent.h"
#include <zyre.h>
#include <zmq.hpp>
#include <atomic>
#include <thread>
#include <map>
#include <list>
#include <queue>

class NetworkManager : public Observable<PlasterEvent> {

private:
    /**
     * The object representing the node in the zyre framework
     */
    zyre_t *node;

    /**
     * The user-friendly name of the node
     */
    std::string name;

    /**
     * A flag indicating whether the node should be running or not
     */
    std::atomic<bool> isRunning;

    /**
     * The object storing the execution thread of the dispatcher
     */
    std::thread listenerTask;

    /**
     * The object storing the execution thread of the dispatcher
     */
    std::thread dispatcherTask;

    RoutingTable routingTable;
    NodeIdMapper nodeIdMapper{};

    /**
     * A queue used to manage the messages that have to be sent,
     * where each element is a pair containing the event to dispatch and the recipients (empty if broadcast)
     */
    std::queue<pair<PlasterEvent *, std::list<std::string>>> eventsBuffer;

    /**
     * Continuously listen for messages exchanged on the network and handle them accordingly
     * @param znode The object representing the zyre node
     */
    void listener(zyre_t *znode);


    /**
     * Continuously check for messages to be sent and publish them on the network
     * @param znode The object representing the zyre node
     */
    void dispatcher(zyre_t *znode);

    /**
     * Handle an incoming message from the network
     * @param msg The object representing the message
     */
    void handleMessage(zmsg_t *msg);

    void selfHandleMessage(PlasterEvent *event);

public:

    /**
     * Create a new NetworkManager with the provided name
     * @param name The string containing the name of the node, not empty
     * @param networkInterface The string containing the network interface to use
     */
    explicit NetworkManager(const std::string &name, const std::string &networkInterface);

    ~NetworkManager();

    /**
     * Stop the node from listening to messages in the network
     */
    void stop();

    /**
     * Sends the provided event to other peers in the network
     * @param event the object representing the event to fire
     * @param recipients the list of strings containing the identifier the event is addressed to, empty to broadcast
     */
    void sendEvent(PlasterEvent *event, std::list<std::string> recipients);

    /**
     * Broadcasts the provided event to other peers in the network
     * @param event The object representing the event to fire
     */
    void broadcastEvent(PlasterEvent *event);

    /**
     * Send the provided file to the provided node in the network
     * @param filePath the path of the file to send
     * @param nodeId the identifier of the node in the network
     */
    void sendFile(const std::string &filePath, const std::string &nodeId);

    /**
     * Retrieves the number of active peers discovered in the network
     * @return the number of active peers in the network
     */
    int getPeersCount();
};


#endif //PLASTER_NETWORKMANAGER_H
