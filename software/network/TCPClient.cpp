#include "TCPClient.h"

void TCPClient::handle_resolve(const boost::system::error_code &err, tcp::resolver::iterator endpoint_iterator) {
    if (!err) {
        tcp::endpoint endpoint = *endpoint_iterator;
        socket_.async_connect(endpoint,
                              boost::bind(&TCPClient::handle_connect, this, boost::asio::placeholders::error,
                                          ++endpoint_iterator));
    } else {
        std::cout << "Error: " << err.message() << '\n';
    }
};

void TCPClient::handle_connect(const boost::system::error_code &err, tcp::resolver::iterator endpoint_iterator) {
    if (!err) {
        boost::asio::async_write(socket_, request_, boost::bind(&TCPClient::handle_write_file, this,
                                                                boost::asio::placeholders::error));
    } else if (endpoint_iterator != tcp::resolver::iterator()) {
        socket_.close();
        tcp::endpoint endpoint = *endpoint_iterator;
        socket_.async_connect(endpoint,
                              boost::bind(&TCPClient::handle_connect, this, boost::asio::placeholders::error,
                                          ++endpoint_iterator));
    } else {
        std::cout << "Error: " << err.message() << '\n';
    };
}

void TCPClient::handle_write_file(const boost::system::error_code &err) {
    if (!err) {
        if (source_file)
            //if(source_file.eof() == false)
        {
            source_file.read(buf.c_array(), (std::streamsize) buf.size());
            if (source_file.gcount() <= 0) {
                std::cout << "read file error" << std::endl;
                return;
            };
            {
                std::cout << "Send " << source_file.gcount() << "bytes, total: " << source_file.tellg() << " bytes.\n";
            }
            boost::asio::async_write(socket_, boost::asio::buffer(buf.c_array(), source_file.gcount()),
                                     boost::bind(&TCPClient::handle_write_file, this,
                                                 boost::asio::placeholders::error));
        } else {
            return;
        }
    } else {
        std::cout << "Error: " << err.message() << "\n";
    }
};

TCPClient::TCPClient(boost::asio::io_service &io_service, const std::string &server, const std::string &path)
        : resolver_(io_service), socket_(io_service) {

    size_t pos = server.find(':');
    if (pos == std::string::npos) {
        return;
    }
    std::string port_string = server.substr(pos + 1);
    std::string server_ip_or_host = server.substr(0, pos);
    source_file.open(path, std::ios_base::binary | std::ios_base::ate);
    if (!source_file) {
        std::cout << __LINE__ << "Failed to open " << path << std::endl;
        return;
    }
    size_t file_size = source_file.tellg();
    source_file.seekg(0);
    std::ostream request_stream(&request_);
    request_stream << path << "\n" << file_size << "\n\n";
    {
        std::cout << "Request size: " << request_.size() << std::endl;
    }
    tcp::resolver::query query(server_ip_or_host, port_string);
    resolver_.async_resolve(query,
                            boost::bind(&TCPClient::handle_resolve, this, boost::asio::placeholders::error,
                                        boost::asio::placeholders::iterator));
}
