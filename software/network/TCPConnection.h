#ifndef PLASTER_TCPCONNECTION_H
#define PLASTER_TCPCONNECTION_H


#include <boost/smart_ptr/enable_shared_from_this.hpp>
#include <boost/asio/streambuf.hpp>
#include <fstream>
#include <boost/array.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

class TCPConnection : public boost::enable_shared_from_this<TCPConnection> {
public:
    TCPConnection(boost::asio::io_service &io_service)
            : socket_(io_service), file_size(0) {
        // Do nothing
    };

    void start();

    boost::asio::ip::tcp::socket& socket();

private:
    boost::asio::streambuf request_buf;
    std::ofstream output_file;
    boost::asio::ip::tcp::socket socket_;
    size_t file_size;
    boost::array<char, 40960> buf;

    void handle_read_request(const boost::system::error_code &err, std::size_t bytes_transferred);

    void handle_read_file_content(const boost::system::error_code &err, std::size_t bytes_transferred);
};


#endif //PLASTER_TCPCONNECTION_H
