#include "BoardResourcesManager.h"
#include <yaml-cpp/yaml.h>
#include <iostream>


BoardResourcesManager::BoardResourcesManager(const string &configFilePath) {

    if (configFilePath.empty()) {
        throw invalid_argument("Config file path cannot be an empty string");
    }

    // First open the file path
    YAML::Node config = YAML::LoadFile(configFilePath);
    // Check whether all the needed nodes are present in the document
    if (!config["resources"] || !config["resources"]["cpu"] || !config["resources"]["fpga"]) {
        throw std::runtime_error("Config file does not contain resources information");
    }
    // If all of them are available, initialize available resources with their value
    this->availableCPUs = config["resources"]["cpu"].as<double>();
    this->availableFPGAs = config["resources"]["fpga"].as<int>();

    std::cout << "[RESOURCES_MANAGER] Initialized resources: CPU: " << this->availableCPUs << " FPGA: "
              << this->availableFPGAs << std::endl;
}

double BoardResourcesManager::getAvailableCPUs() const {
    return availableCPUs;
}

int BoardResourcesManager::getAvailableFPGAs() const {
    return availableFPGAs;
}

bool BoardResourcesManager::bookResources(const TaskView &task, double cpuCount, int fpgaCount) {

    if (cpuCount < 0 || fpgaCount < 0) {
        throw invalid_argument("Found negative parameters");
    }

    // Check whether there are enough resources
    if (cpuCount > this->availableCPUs || fpgaCount > this->availableFPGAs) {
        // If not just stop
        return false;
    }

    // And then decrease the count of available ones, by getting a lock on them
    unique_lock<mutex> cpuLock(cpuMutex);
    unique_lock<mutex> fpgaLock(fpgaMutex);
    unique_lock<mutex> allocationLock(allocationMutex);
    // First create the identifier of the allocation
    std::string identifier = task.getAppName() + task.getTaskName();
    // Then check whether resources have already been allocated to that combo of app / task
    auto allocationIterator = this->resourceAllocation.find(identifier);
    if (allocationIterator != this->resourceAllocation.end()) {
        // TODO: Check what to do when resources for the task have already been allocated
        return false;
    }
    // If not first create the allocation entry
    BoardResourcesManager::ResourceQuotaEntry allocationEntry(cpuCount, fpgaCount);
    // Then decrease the counters
    this->availableCPUs -= cpuCount;
    this->availableFPGAs -= fpgaCount;
    // And eventually insert the allocation entry into the log
    this->resourceAllocation.insert(
            std::pair<std::string, ResourceQuotaEntry>(identifier, allocationEntry));
    return true;
}

void BoardResourcesManager::freeResources(const TaskView &task) {
    // Increase the count of available CPUs, by getting a lock on them
    unique_lock<mutex> cpuLock(cpuMutex);
    unique_lock<mutex> fpgaLock(fpgaMutex);
    unique_lock<mutex> allocationLock(allocationMutex);
    // First try to find the allocation for the provided task
    std::string identifier = task.getAppName() + task.getTaskName();
    auto allocationIterator = this->resourceAllocation.find(identifier);
    // Check whether an allocation exists
    if (allocationIterator == this->resourceAllocation.end()) {
        // If not just stop
        return;
    }
    // Then first increase the counters with the allocated resources
    this->availableCPUs = allocationIterator->second.getCpuQuota();
    this->availableFPGAs = allocationIterator->second.getFpgaQuota();
    // And then remove the entry
    this->resourceAllocation.erase(allocationIterator);
}

BoardResourcesManager::ResourceQuotaEntry::ResourceQuotaEntry(double cpuQuota, int fpgaQuota) : cpuQuota(cpuQuota),
                                                                                                fpgaQuota(fpgaQuota) {
}

double BoardResourcesManager::ResourceQuotaEntry::getCpuQuota() const {
    return cpuQuota;
}

int BoardResourcesManager::ResourceQuotaEntry::getFpgaQuota() const {
    return fpgaQuota;
}
