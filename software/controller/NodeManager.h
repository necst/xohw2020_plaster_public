#ifndef PLASTER_NODEMANAGER_H
#define PLASTER_NODEMANAGER_H

#include <queue>
#include <string>
#include <list>
#include "../utils/Observer.h"
#include "../events/EventsVisitor.h"
#include "../api/ApiServer.h"
#include "../api/ApiCommandsVisitor.h"
#include "../network/NetworkManager.h"
#include "Task.h"
#include "BoardResourcesManager.h"
#include "TaskLaunchOutcome.h"
#include "TaskLaunchOutcomeManager.h"
#include "TaskAvailabilityResult.h"
#include "TaskAvailabilityManager.h"
#include "../execution/TaskExecutor.h"
#include "NetworkManagerDelegate.h"
#include "AppLauncher.h"

class PlasterEvent;

/**
 * A class specializing Task that describes an object that manages the current node, keeping information about the
 * resources and handling the events received from other nodes in the network.
 * It implements the EventsVisitor interface to dynamically handle received events and the Observer interface to be
 * notified by the NetworkManager about incoming events.
 */
class NodeManager
        : public Observer<PlasterEvent>, public Observer<TaskLaunchOutcome>,
          public NetworkManagerDelegate,
          public Observer<ApiCommand>,
          public EventsVisitor, public ApiCommandsVisitor, public Task {
private:

    std::string configFile;
    std::queue<std::string> outputQueue;

    NetworkManager networkManager;
    BoardResourcesManager resourcesManager;
    ApiServer apiServer{};
    /**
     * A map associating each task to its identifier
     */
    map<std::string, map<std::string, TaskExecutor &>> executionPool;
    map<std::string, std::chrono::steady_clock::time_point> transferTimes{};
    /**
     * A map associating each task to its launch outcome manager
     */
    std::map<std::string, AppLauncher *> appLaunchers{};
    std::map<std::string, std::map<std::string, TaskLaunchOutcomeManager>> taskLaunchOutcomeManagers{};
    std::map<std::string, std::map<std::string, TaskAvailabilityManager>> taskAvailabilityManagers{};
    std::string nodeId;

    /**
     * Retrieves the executor of the provided task
     * @param appId the identifier of the application, not empty
     * @param taskName the name of the task, not empty
     * @return the object representing the task executor, null if the task or the app is not present
     */
    TaskExecutor *getTaskExecutor(const std::string &appId, const std::string &taskName);

    /**
     * Retrieves all the tasks associated to the provided application
     * @param appId the identifier of the application, not empty
     * @return a map associating each identifier to the object representing the task executor, empty if the application is not present
     */
    std::map<std::string, TaskExecutor &> getAppTasks(const std::string &appId) const;

public:

    /**
     * Create a new instance of the node manager

     * @param nodeId The identifier of the node in the network, not empty
     * @param The name of the node manager, not empty
     * @param appName The name of the app the manager belongs to, not empty
     * @param networkInterface The string containing the network interface to use
     */
    explicit NodeManager(string &nodeId,
                         string taskName, 
                         string appName, 
                         string &networkInterface, 
                         string &configFile);

    /**
     * Add an application and handle its launch onto the distributed system.
     * @param appFilePath The path where the app source code and configuration files are stored
     */
    void addAppFromLocal(std::string &appFilePath);

    /**
     * Delete the provided app from the system
     * @param appId The identifier of the app in the system
     */
    void deleteApp(std::string &appId);

    /**
     * Restart the provided app
     * @param appId The identifier of the app in the system
     */
    void restartApp(std::string &appId);

    /**
     * Ask the tasks belonging to the provided app to start/stop logging their activity
     * @param appId The identifier of the app in the system
     * @param isCollecting {@code true} if the tasks should log, {@code false} otherwise
     */
    void setDistributedOutputCollection(std::string &appId, bool isCollecting);

    /**
     * Ask the tasks belonging to the provided app to start/stop logging their activity
     * @param appId The identifier of the app in the system
     * @param isCollecting {@code true} if the tasks should log, {@code false} otherwise
     * @param collector The identifier of the node that should collect the output
     */
    void setLocalOutputCollection(std::string &appId, bool isCollecting, std::string &collector);

    /**
     * Retrieve logs of the provided app from all the nodes
     * @param appId The identifier of the app in the system
     */
    void consumeOutput(std::string &appId);

    void onMessageReceived(std::string sender, std::string messageType, std::string messagePayload) override;

/* ===== TASK MANAGEMENT ===== */
    void setup() override;

    void run() override;

    void stop() override;

/* ===== EVENTS VISITOR ===== */
    void handle(AbortLaunchTask &task) override;

    void handle(KillApp &app) override;

    void handle(AskForAvailability &availability) override;

    void handle(CollectOutput &output) override;

    void handle(LaunchCompleted &completed) override;

    void handle(LaunchTask &task) override;

    void handle(NodeAvailability &availability) override;

    void handle(SendOutput &output) override;

    void handle(FileRequestEvent &event) override;

    void handle(FileChunkEvent &event) override;

    void handle(TaskCommunication &communication) override;

/* ===== OBSERVER ===== */
    void update(PlasterEvent &data) override;

    void update(TaskLaunchOutcome &data) override;

    void update(ApiCommand &data) override;

    /* ===== NETWORK MANAGER DELEGATE METHODS ===== */
    void broadcastEvent(PlasterEvent *event) override;

    void sendEvent(PlasterEvent *event, std::list<std::string> recipients) override;

    /* ===== COMMANDS VISITOR ===== */
    void handle(StartAppCommand &command) override;

    void handle(KillAppCommand &command) override;

    void handle(RestartAppCommand &command) override;

    void handle(SetOutCollectionCommand &command) override;

    void handle(ConsumeOutputCommand &command) override;

    void handle(TransferFileCommand &command) override;

};


#endif //PLASTER_NODEMANAGER_H
