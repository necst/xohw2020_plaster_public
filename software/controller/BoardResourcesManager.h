
#ifndef PLASTER_BOARDRESOURCESMANAGER_H
#define PLASTER_BOARDRESOURCESMANAGER_H

#include <string>
#include <mutex>
#include <map>
#include "TaskView.h"

using namespace std;

/**
 * A class representing an object that stores information about the resources available on the board.
 * The information is initialized by providing a configuration file
 */
class BoardResourcesManager {

public:
    /**
     * Creates an instance of the resources manager, initializing the information from the provided config file
     * @param configFilePath the path in which the config file is stored, not null and not empty
     */
    explicit BoardResourcesManager(const string &configFilePath);

    /**
     * Retrieves the number of available CPUs on the device
     * @return the number of available CPUs on the device
     */
    double getAvailableCPUs() const;

    /**
     * Retrieves the number of available FPGAs on the device
     * @return the number of available FPGAs on the device
     */
    int getAvailableFPGAs() const;

    /**
     * Reserves a certain number of CPUs for the execution of a task
     * throws an exception if the required resources exceed the available ones
     * @param task the object representing the task, not null
     * @param cpuCount the number of CPUs to reserve, not negative
     * @param fpgaCount the number of FPGAs to reserve, not negative
     */
    bool bookResources(const TaskView &task, double cpuCount, int fpgaCount);

    /**
     * Frees the resources that are no longer needed for the execution of the provided task
     * @param task the object representing the task, not null
     */
    void freeResources(const TaskView &task);

private:
    class ResourceQuotaEntry {
        double cpuQuota;
        int fpgaQuota;
    public:
        ResourceQuotaEntry(double cpuQuota, int fpgaQuota);

        double getCpuQuota() const;

        int getFpgaQuota() const;
    };

    /**
     * The count of available CPUs on the device
     */
    double availableCPUs;

    /**
     * The count of available FPGAs on the device
     */
    int availableFPGAs;
    /**
     * The log of how resources are allocated w.r.t. tasks
     */
    std::map<std::string, BoardResourcesManager::ResourceQuotaEntry> resourceAllocation{};

    mutex cpuMutex;
    mutex fpgaMutex;
    mutex allocationMutex;
};


#endif //PLASTER_BOARDRESOURCESMANAGER_H
