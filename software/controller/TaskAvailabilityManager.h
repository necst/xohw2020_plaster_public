#ifndef PLASTER_TASKAVAILABILITYMANAGER_H
#define PLASTER_TASKAVAILABILITYMANAGER_H

#include <string>
#include "../utils/Observable.h"
#include "BinaryRepliesManager.h"
#include "TaskAvailabilityResult.h"
#include "TaskView.h"

/**
 * A class used to store information about the availability of peers
 * for the launch of a certain task.
 * It implements the observer pattern to notify observers about the
 * availability of peers for a certain task.
 */
class TaskAvailabilityManager : public BinaryRepliesManager, public Observable<TaskAvailabilityResult> {
//TODO: Add observable

public:
    /**
     * Creates a new instance of the availability manager
     * @param taskView the object representing the task's information
     * @param nodesCount the number of nodes that are expected to reply, not negative
     * @param requestedAvailableNodes the minimum number of nodes that should report availability, not negative
     */
    TaskAvailabilityManager(const TaskView& taskView, int nodesCount, int requestedAvailableNodes);

    void checkCompletion() override;

private:
    const int requestedAvailableNodes;
    const TaskView taskView;

};


#endif //PLASTER_TASKAVAILABILITYMANAGER_H
