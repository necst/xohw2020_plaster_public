#include "Task.h"

void Task::stop() {
    // Set the liveness flag as false
    isAlive = false;
}
