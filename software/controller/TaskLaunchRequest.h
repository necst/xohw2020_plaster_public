
#ifndef PLASTER_TASKLAUNCHREQUEST_H
#define PLASTER_TASKLAUNCHREQUEST_H

#include <string>
#include <list>

using namespace std;

/**
 * A class representing an object that is used when a task needs to be launched but is still waiting for the
 * availability of enough nodes to carry out its execution.
 * It holds information about the peers that replied positively to the request, ordering them by reply time
 */
class TaskLaunchRequest {
public:

    /**
     * Creates a new instance of the object, with the provided number of required nodes
     * @param requiredNodes the number of required nodes, not negative
     * @param peersCount the total number of peers in the system (and hence expected replies), not negative
     */
    explicit TaskLaunchRequest(int requiredNodes, int peersCount);

    /**
     * Adds a new reply and checks whether the execution requirements have been met
     * @param peerName the name of the peer that sent the reply add, not null and not empty
     * @param isAvailable {@code true} if the peer has replied positively, {@code false} otherwise
     * @return {@code true} if the requirements are met, {@code false} otherwise
     */
    bool addReply(string &peerName, bool isAvailable);

    /**
     * Updates the number of total peers in the system
     * @param peersCount the number of total peers in the system, not negative
     */
    void updatePeersCount(int peersCount);

private:
    /**
     * The number of replies received so far by the other peers
     */
    int receivedReplies;
    /**
     * The number of nodes that sent a positive reply and are hence available for task execution
     */
    int availableNodes;
    /**
     * The list of the IDs of the nodes that sent a positive reply and are hence available for task execution
     */
    list <string> availablePeers;
    /**
     * The number of nodes that are required for the execution
     */
    const int requiredNodes;
    /**
     * The total count of peers in the system, equal to the number of expected replies
     */
    int peersCount;
};


#endif //PLASTER_TASKLAUNCHREQUEST_H
