#ifndef PLASTER_TASKVIEW_H
#define PLASTER_TASKVIEW_H

#include <string>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/export.hpp>

using namespace boost::archive;


/**
 * A class representing a simplified version of a task, holding just the essential information for its management
 */
class TaskView {

public:

    TaskView() = default;

    /**
     * Creates a new instance holding the provided task information
     * @param appName the name of the app the task belongs to, not empty
     * @param taskName the name of the task, not empty
     * @param replicas the number of replicas of the task, not negative
     */
    explicit TaskView(std::string &appName, std::string &taskName, int replicas);

    /**
     * Creates a new instance holding the provided task information
     * @param appName the name of the app the task belongs to, not empty
     * @param taskName the name of the task, not empty
     * @param replicas the number of replicas of the task, not negative
     * @param codeDir the directory where the task code is stored, not empty
     * @param codeFile the main file containing the source code of the task, not empty
     */
    explicit TaskView(std::string &appName, std::string &taskName, int replicas, std::string &codeDir,
                      std::string &codeFile);

    const std::string &getAppName() const;

    const std::string &getTaskName() const;

    const std::string &getCodeDir() const;

    const std::string &getCodeFile() const;

    double getCpuQuota() const;

    void setCpuQuota(double cpuQuota);

    int getFpgaQuota() const;

    void setFpgaQuota(int fpgaQuota);

    int getReplicas() const;

private:
    friend class boost::serialization::access;

    std::string appName;
    std::string taskName;
    std::string codeDir;
    std::string codeFile;
    double cpuQuota;
    int fpgaQuota;
    int replicas;

    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar;
        ar & appName;
        ar & taskName;
        ar & codeDir;
        ar & codeFile;
        ar & replicas;
        ar & cpuQuota;
        ar & fpgaQuota;
    };
};

BOOST_CLASS_EXPORT_KEY(TaskView)


#endif //PLASTER_TASKVIEW_H
