

#include "TaskAvailabilityManager.h"

TaskAvailabilityManager::TaskAvailabilityManager(const TaskView &taskView, int nodesCount, int requestedAvailableNodes)
        : BinaryRepliesManager(taskView, nodesCount),
          taskView(taskView),
          requestedAvailableNodes(requestedAvailableNodes) {
    if (requestedAvailableNodes < 0) {
        throw std::domain_error("Requested available nodes count cannot be negative");
    }
}

void TaskAvailabilityManager::checkCompletion() {
    // We check whether we received all the required replies
    if (repliesCount == nodesCount) {
        // We first create an object for the result containing the list of available peers
        TaskAvailabilityResult availabilityResult{taskView, positiveNodes, requestedAvailableNodes};
        // And send it to attached observers
        notifyObservers(availabilityResult);
    }
}
