#include "AppLauncher.h"
#include "../events/LaunchTask.h"
#include "../events/AskForAvailability.h"

#include <utility>


AppLauncher::AppLauncher(std::vector<TaskView> appTasks, int peersCount, std::string nodeId) : pendingTasks(appTasks),
                                                                                               nodeId(nodeId) {
    // We create availability managers for all the tasks and start sending events
    for (auto taskIterator = pendingTasks.begin(); taskIterator != pendingTasks.end(); taskIterator++) {
        std::cout << "Creating availability manager for task " << taskIterator->getTaskName() <<
                  " required nodes: " << taskIterator->getReplicas() << std::endl;
        availabilityManagers.insert(
                std::pair<std::string, TaskAvailabilityManager>(
                        taskIterator->getTaskName(),
                        TaskAvailabilityManager(
                                *taskIterator,
                                peersCount,
                                taskIterator->getReplicas())
                )
        );
        pendingTasksCount++;
    }
    for (auto managerIterator = availabilityManagers.begin();
         managerIterator != availabilityManagers.end(); managerIterator++) {
        // Attach as the observer of all the availability managers
        managerIterator->second.addObserver(this);
    }
}

AppLauncher::~AppLauncher() {
    std::cout << "Called the destructor" << std::endl;
    this->workerTask.join();
}

void AppLauncher::launchApp() {
    int replicaCount;

    std::cout << "Started launching app" << std::endl;

    // Send a bunch of availability messages, to query the peers
    for (auto taskIterator = pendingTasks.begin(); taskIterator != pendingTasks.end(); taskIterator++) {
        for (replicaCount = 0; replicaCount < taskIterator->getReplicas(); replicaCount++) {
            PlasterEvent *availabilityRequestEvent = new AskForAvailability(
                    this->nodeId,
                    *taskIterator,
                    taskIterator->getCpuQuota(),
                    taskIterator->getFpgaQuota()
            );
            nmDelegate->broadcastEvent(availabilityRequestEvent);
        }
    }

    workerTask = std::thread(&AppLauncher::coreWorker, this);
}

void AppLauncher::notifyAvailability(NodeAvailability availabilityReply) {
    std::cout << "Received availability for task " << availabilityReply.getTask().getTaskName() << std::endl;
    // We add the incoming reply to the buffer
    repliesBuffer.push(availabilityReply);
}

void AppLauncher::coreWorker() {
    // Repeat until all tasks have been correctly assigned
    while (pendingTasksCount > 0) {
        // Check whether there are replies in the buffer
        if (!repliesBuffer.empty()) {
            NodeAvailability reply = repliesBuffer.front();
            auto taskIterator = availabilityManagers.find(reply.getTask().getTaskName());
            if (taskIterator == availabilityManagers.end()) {
                // If the task is not tracked, ignore this
                continue;
            }
            // Add the reply to the availability manager
            taskIterator->second.addReply(reply.getSender(), reply.isNodeAvailable());
            // And remove the reply from the buffer
            repliesBuffer.pop();
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

void AppLauncher::update(TaskAvailabilityResult &data) {
    std::cout << "Received availability for " << data.getTask().getTaskName() << " of " << data.getTask().getAppName()
              << std::endl;

    auto taskIterator = availabilityManagers.find(data.getTask().getTaskName());
    if (taskIterator == availabilityManagers.end()) {
        std::cout << "Received availability for task " << data.getTask().getTaskName() << " but was not pending";
        return;
    }

    // Then check whether there is at least one available node
    if (data.getAvailableNodes().empty()) {
        std::cout << "No available nodes found to execute task " << data.getTask().getTaskName() << " for app "
                  << data.getTask().getAppName();
        // TODO: Check whether to return anything
        return;
    }
    // Then we should create a manager for the completions
    auto outcomeManagerIterator = launchManagers.find(data.getTask().getTaskName());
    if (outcomeManagerIterator != launchManagers.end()) {
        launchManagers.erase(outcomeManagerIterator);
    }
    // Create a manager for the given task, with the given amount of replicas
    TaskLaunchOutcomeManager outcomeManager(data.getTask(), data.getRequestedAvailableNodes());
    outcomeManager.addObserver(this);
    launchManagers.insert(std::pair<std::string, TaskLaunchOutcomeManager>(
            data.getTask().getTaskName(),
            outcomeManager));
    // TODO: Send launch request to all the available nodes until the replica amount is met
    // Once we assert to have at least one available node, pick one from the set
    std::string candidateNodeId = *(data.getAvailableNodes().begin());
    assignedTasks.insert(std::pair<std::string, TaskView>(candidateNodeId, data.getTask()));
    // Then send a launch task to the selected candidate and an abort to all the others
    PlasterEvent *launchTaskEvent = new LaunchTask(nodeId, data.getTask());
    auto abortLaunchEvent = new LaunchTask(nodeId, data.getTask());
    std::cout << "sending launch event to " << candidateNodeId << std::endl;
    nmDelegate->sendEvent(launchTaskEvent, std::list<std::string>{candidateNodeId});
    // Then construct a list of all the other nodes, excluding the one selected for the execution
    // This is achieved by creating a new list from the set starting from the element following the set beginning
    //std::list<std::string> abortRecipients(++(data.getAvailableNodes().begin()), data.getAvailableNodes().end());
    // And eventually tell them to abort the launch and free the reserved resources
    //networkManager.sendEvent(abortLaunchEvent, abortRecipients);

    // TODO: Eventually, remove the availability manager from the map and destroy the availability manager
    //free(&taskIterator->second);
    //appIterator->second.erase(taskIterator);
}

void AppLauncher::update(TaskLaunchOutcome &data) {
    pendingTasksCount--;
    // Just propagate the outcome to observers
    notifyObservers(data);
}

void AppLauncher::setNetworkManager(NetworkManagerDelegate *nmDelegate) {
    AppLauncher::nmDelegate = nmDelegate;
}
