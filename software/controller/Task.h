#ifndef PLASTER_TASK_H
#define PLASTER_TASK_H

#include <string>

/**
 * An abstract class describing a task running inside the system.
 * Each actual task executed in the system will extend this base class.
 */
class Task {

public:
    /**
     * Creates a new task
     */
    Task() = default;

    ~Task() = default;

    /**
     * Set up the task, by loading for instance the needed configuration and bit-stream.
     * It should be re-implemented by each subclass
     */
    virtual void setup() = 0;

    virtual void run() = 0;

    virtual void stop();

    /**
     * Reacts to an incoming message from other peers executing the app
     * @param sender the identifier of the node that sent the message
     * @param messageType the identifier of the type of message (defined by the user)
     * @param messagePayload the actual content of the message, in a JSON string
     */
    virtual void onMessageReceived(std::string sender, std::string messageType, std::string messagePayload) = 0;

    bool isAlive{true};
};


#endif //PLASTER_TASK_H
