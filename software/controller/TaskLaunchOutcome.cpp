

#include <stdexcept>
#include "TaskLaunchOutcome.h"

TaskLaunchOutcome::TaskLaunchOutcome(const TaskView &taskView, bool launchSuccessful)
        : taskView(taskView), launchSuccessful(launchSuccessful) {
}

const TaskView &TaskLaunchOutcome::getTask() const {
    return taskView;
}

bool TaskLaunchOutcome::isLaunchSuccessful() const {
    return launchSuccessful;
}
