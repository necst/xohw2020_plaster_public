#ifndef PLASTER_APPLAUNCHER_H
#define PLASTER_APPLAUNCHER_H

#include <map>
#include <list>
#include <queue>
#include <thread>
#include <mutex>

#include "TaskLaunchOutcome.h"
#include "../utils/Observable.h"
#include "../events/NodeAvailability.h"
#include "TaskAvailabilityManager.h"
#include "TaskLaunchOutcomeManager.h"
#include "../network/NetworkManager.h"
#include "NetworkManagerDelegate.h"

/**
 * A class representing an object used to manage the launch of an app in the system.
 * It handles all the steps from querying the availability of nodes to the selection of the candidate ones
 * to execute the tasks composing the app.
 */
class AppLauncher
        : public Observable<TaskLaunchOutcome>,
          public Observer<TaskAvailabilityResult>,
          public Observer<TaskLaunchOutcome> {

public:
    explicit AppLauncher(std::vector<TaskView> appTasks, int peersCount, std::string nodeId);

    ~AppLauncher();

    /**
     * Starts the process to run the provided app in the system
     */
    void launchApp();

    /**
     * Notify the module about an incoming reply to an availability query
     * @param availabilityReply the object representing the reply
     */
    void notifyAvailability(NodeAvailability availabilityReply);

    void setNetworkManager(NetworkManagerDelegate *nmDelegate);

    void update(TaskAvailabilityResult &data) override;

    void update(TaskLaunchOutcome &data) override;


private:
    std::string nodeId;
    NetworkManagerDelegate *nmDelegate;
    int pendingTasksCount = 0;
    /**
     * A map associating each task to the object managing the corresponding availability query
     */
    std::map<std::string, TaskAvailabilityManager> availabilityManagers{};
    std::map<std::string, TaskLaunchOutcomeManager> launchManagers{};
    std::queue<NodeAvailability> repliesBuffer{};
    std::vector<TaskView> pendingTasks;
    std::map<std::string, TaskView> assignedTasks{};

    std::thread workerTask;

    //TODO: Add a way to keep track of pending tasks to send again availability requests after a while

    void coreWorker();

};


#endif //PLASTER_APPLAUNCHER_H
