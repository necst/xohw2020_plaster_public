#ifndef PLASTER_TASKAVAILABILITYRESULT_H
#define PLASTER_TASKAVAILABILITYRESULT_H

#include <string>
#include <set>
#include "TaskView.h"

/**
 * A class used the represent an object containing the nodes that have been identified as available
 * for the execution of a certain task belonging to a certain application
 */
class TaskAvailabilityResult {
public:
    /**
     * Creates a new instance for the result of the provided task, containing the available nodes
     * @param the object representing the task's information
     * @param availableNodes a set of strings containing the identifiers of the available nodes
     * @param requestedAvailableNodes the minimum number of nodes that were requested to report availability, not negative
     */
    TaskAvailabilityResult(TaskView taskView, std::set<std::string> availableNodes, int requestedAvailableNodes);

    std::set<std::string> getAvailableNodes() const;

    int getRequestedAvailableNodes() const;

    const TaskView &getTask() const;

private:
    const TaskView taskView;
    const int requestedAvailableNodes;
    const std::set<std::string> availableNodes;
};


#endif //PLASTER_TASKAVAILABILITYRESULT_H
