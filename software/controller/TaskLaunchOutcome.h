#ifndef PLASTER_TASKLAUNCHOUTCOME_H
#define PLASTER_TASKLAUNCHOUTCOME_H

#include <string>
#include "TaskView.h"

/**
 * A class representing the outcome of a task launch
 */
class TaskLaunchOutcome {
public:
    /**
     * Creates a new instance, holding information about the outcome for the provided task
     * @param taskView the object representing the task's information
     * @param launchSuccessful {@code true} whether the launch was successful, {@code false} otherwise
     */
    TaskLaunchOutcome(const TaskView &taskView, bool launchSuccessful);

    const TaskView &getTask() const;

    bool isLaunchSuccessful() const;

private:
    TaskView taskView;
    bool launchSuccessful;
};


#endif //PLASTER_TASKLAUNCHOUTCOME_H
