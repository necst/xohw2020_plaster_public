

#include <stdexcept>
#include <utility>
#include "TaskAvailabilityResult.h"

TaskAvailabilityResult::TaskAvailabilityResult(TaskView taskView, std::set<std::string> availableNodes,
                                               int requestedAvailableNodes) : taskView(taskView),
                                                                              availableNodes(std::move(
                                                                                      availableNodes)),
                                                                              requestedAvailableNodes(
                                                                                      requestedAvailableNodes) {

}

std::set<std::string> TaskAvailabilityResult::getAvailableNodes() const {
    return std::set<std::string>{availableNodes};
}

const TaskView &TaskAvailabilityResult::getTask() const {
    return taskView;
}

int TaskAvailabilityResult::getRequestedAvailableNodes() const {
    return requestedAvailableNodes;
}
