#ifndef PLASTER_TASKLAUNCHOUTCOMEMANAGER_H
#define PLASTER_TASKLAUNCHOUTCOMEMANAGER_H


#include <set>
#include <string>
#include "../utils/Observable.h"
#include "TaskLaunchOutcome.h"
#include "BinaryRepliesManager.h"

/**
 * A class used to store information about the outcome of the launch of a task.
 * It holds the number of nodes the task was assigned to, along with the number
 * of nodes that successfully launched it.
 */
class TaskLaunchOutcomeManager : public BinaryRepliesManager, public Observable<TaskLaunchOutcome> {

public:
    TaskLaunchOutcomeManager(const TaskView &taskView, int nodesCount);

private:
    /**
     * Checks whether all the required replies have been received
     */
    void checkCompletion() override;

};


#endif //PLASTER_TASKLAUNCHOUTCOMEMANAGER_H
