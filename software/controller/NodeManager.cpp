#include "NodeManager.h"
#include "../events/AbortLaunchTask.h"
#include "../events/SendOutput.h"
#include "../events/NodeAvailability.h"
#include "../events/LaunchCompleted.h"
#include "../events/CollectOutput.h"
#include "../events/KillApp.h"
#include "../events/AskForAvailability.h"
#include "../events/LaunchTask.h"
#include "../events/FileRequestEvent.h"
#include "../events/FileChunkEvent.h"
#include "../api/StartAppCommand.h"
#include "../api/KillAppCommand.h"
#include "../api/ConsumeOutputCommand.h"
#include "../api/SetOutCollectionCommand.h"
#include "../api/TransferFileCommand.h"
#include "../api/RestartAppCommand.h"
#include "TaskView.h"
#include <iostream>
#include <fstream>
#include <yaml-cpp/yaml.h>
#include <boost/asio/io_service.hpp>

using namespace std;

NodeManager::NodeManager(string &nodeId, string taskName, string appName, string &networkInterface, string &configFile) : Task(),
                                                                            nodeId(nodeId),
                                                                            outputQueue(),
                                                                            executionPool(),
                                                                            resourcesManager(configFile),
                                                                            networkManager(nodeId, networkInterface)
{
    if (nodeId.empty())
    {
        throw std::invalid_argument("Node name cannot be empty");
    }
    // Add as an observer of the network manager
    networkManager.addObserver(this);
    // And as an observer of the API server
    apiServer.addObserver(this);
}

void NodeManager::addAppFromLocal(std::string &appFilePath)
{
    // Here we should open the app from the provided location and start scheduling its tasks
    //TODO: Implement this method
}

void NodeManager::deleteApp(std::string &appId)
{
    //TODO: Implement this method
}

void NodeManager::restartApp(std::string &appId)
{
    //TODO: Implement this method
}

void NodeManager::setDistributedOutputCollection(std::string &appId, bool isCollecting)
{
    //TODO: Implement this method
}

void NodeManager::consumeOutput(std::string &appId)
{
    //TODO: Implement this method
}

void NodeManager::setLocalOutputCollection(std::string &appId, bool isCollecting, std::string &collector)
{
    //TODO: Implement this method
}

void NodeManager::handle(AbortLaunchTask &event)
{
    // First ask the resources manager to free the reserved resources for the task
    resourcesManager.freeResources(event.getTask());
    // TODO: Check whether we need to remove the task from somewhere
}

void NodeManager::handle(AskForAvailability &event)
{
    std::cout << "Received availability query event for task " << event.getTask().getTaskName() << std::endl;
    // Here we have to check whether the requested resources are available on the node
    // We hence ask the resources manager about their availability
    bool resourcesAvailable = resourcesManager.getAvailableCPUs() >= event.getRequiredCpu() && resourcesManager.getAvailableFPGAs() >= event.getRequiredFpga();
    // First create the reply event, with the current node as the sender and the task copied from the received event
    auto availabilityEvent = new NodeAvailability(nodeId, TaskView(event.getTask()));
    // Check whether the resources are available
    if (resourcesAvailable)
    {
        // Ask the resources manager to book them
        resourcesManager.bookResources(event.getTask(), event.getRequiredCpu(), event.getRequiredFpga());
    }
    std::cout << "Replying to availability query: " << resourcesAvailable << std::endl;
    // Set the availability flag of the event
    availabilityEvent->setIsNodeAvailable(resourcesAvailable);
    // Eventually ask the network manager to dispatch the event to other nodes in the network
    networkManager.broadcastEvent(availabilityEvent);
}

void NodeManager::handle(CollectOutput &event)
{
    //TODO: Implement this method
}

void NodeManager::handle(KillApp &event)
{
    /*
     * Here we should stop all the tasks associated to the app
     */
    // First load the tasks associated to the application
    auto tasks = getAppTasks(event.getAppId());
    // Get an iterator to scan the pairs, and for each task:
    for (auto &task : tasks)
    {
        // Call the stop method on the task executor
        task.second.stop();
        // And ask the resources manager to free the reserved resources
        resourcesManager.freeResources(task.second.getTask());
        // TODO: Remove the executor from the pool
    }
    // TODO: Check whether to send an additional acknowledgement message
}

void NodeManager::handle(LaunchCompleted &event)
{
    // Here we have to update the data structure keeping track of the launch outcomes
    // First get the manager starting from the app and task name
    auto appIterator = taskLaunchOutcomeManagers.find(event.getTask().getAppName());
    if (appIterator == taskLaunchOutcomeManagers.end())
    {
        // TODO: Decide what to do when the app is not present
        // Since when we start launching the app we put an entry, we should not be in this state
        return;
    }
    // Then we search for the task
    auto taskIterator = appIterator->second.find(event.getTask().getTaskName());
    // And check whether the task is present
    if (taskIterator == appIterator->second.end())
    {
        // TODO: Decide what to do when the task is not present
        return;
    }
    // And eventually get the outcome manager
    auto taskOutcomeManager = taskIterator->second;
    // And add the received outcome
    taskOutcomeManager.addReply(event.getSender(), event.isLaunchSuccessful());
}

void NodeManager::handle(LaunchTask &event)
{
    std::cout << "Received launch event for task " << event.getTask().getTaskName() << std::endl;

    //TODO: Consider how to pass files required for the task
    // We should download the app files from the node
    std::string filePath = event.getTask().getCodeDir() + "/" + event.getTask().getCodeFile();
    // TODO: Check whether the reset the file content
    PlasterEvent *fileRequestEvent = new FileRequestEvent(nodeId, filePath,
                                                          event.getTask().getAppName() + event.getTask().getTaskName());
    networkManager.sendEvent(fileRequestEvent, std::list<std::string>{event.getSender()});

    // We first check whether an executor of the task already exists
    auto appName = event.getTask().getAppName();
    std::string taskName = event.getTask().getTaskName();
    auto appPoolIterator = executionPool.find(appName);
    if (appPoolIterator == executionPool.end())
    {
        // If the app is not already present we add it
        executionPool.insert(std::pair<std::string, std::map<std::string, TaskExecutor &>>(
            appName,
            std::map<std::string, TaskExecutor &>()));
        // And update the reference to the newly inserted pair
        appPoolIterator = executionPool.find(appName);
    }
    // Then search for the executor of the task
    auto taskIterator = appPoolIterator->second.find(taskName);
    if (taskIterator != appPoolIterator->second.end())
    {
        // If it already exists, we remove it
        taskIterator->second.stop();
        appPoolIterator->second.erase(taskIterator);
    }
    // Create an executor to run the task contained in the event
    TaskExecutor taskExecutor(event.getTask(), nodeId);
    // Provide it the reference to the network manager to dispatch events
    taskExecutor.setNetworkManager(&networkManager);
    // And insert it into the map of the app
    appPoolIterator->second.insert(
        std::pair<std::string, TaskExecutor &>(taskName, taskExecutor));
    // Eventually, start the execution
    taskExecutor.start();
    // Then create an acknowledgement telling that launch completed successfully
    PlasterEvent *launchCompletedEvent = new LaunchCompleted(this->nodeId, event.getTask(), true);
    // And send it to the node that requested the task launch
    networkManager.sendEvent(launchCompletedEvent, std::list<std::string>{event.getSender()});
    taskExecutor.stop();
}

void NodeManager::handle(NodeAvailability &event)
{
    // Find the app launcher associated to the task
    auto appIterator = appLaunchers.find(event.getTask().getAppName());
    if (appIterator == appLaunchers.end()) {
        // If the app is not pending, just stop
        return;
    }
    // Then notify the reply to the launcher
    appIterator->second->notifyAvailability(event);
}

void NodeManager::handle(SendOutput &event)
{
    //TODO: Implement this method
}

void NodeManager::update(PlasterEvent &data)
{
    // When receiving an event, visit it
    data.accept(*this);
}

void NodeManager::setup()
{
    // This sets the node manager up
    // First open the config file and read the available resources and configurations
    // TODO: Set the number of available resources
    // TODO: Set the runtime version of python
    // TODO: Set log for apps

    // Then gets a reference to the zmq context ? Maybe not in our case

    // Eventually creates the objects for the events (why?)
    // And registers the events
}

void NodeManager::run()
{
    while (this->isAlive)
    {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    //TODO: Implement this method
}

void NodeManager::stop()
{
    // First we should stop the execution of all the apps and tasks
    for (auto &app : executionPool)
    {
        for (auto &task : app.second)
        {
            std::cout << "[NODE_MANAGER] Stopping execution of " << task.second.getTask().getAppName() << " / "
                      << task.second.getTask().getTaskName();
            // Ask the task to stop
            task.second.stop();
            // Then free the resources used by the task
            resourcesManager.freeResources(task.second.getTask());
        }
    }
    // Then call the superclass method to stop the execution
    Task::stop();
}

std::map<std::string, TaskExecutor &> NodeManager::getAppTasks(const string &appId) const
{
    if (appId.empty())
    {
        throw std::invalid_argument("App identifier cannot be empty");
    }
    // Try to find the entry for the application
    auto appIterator = executionPool.find(appId);
    if (appIterator == executionPool.end())
    {
        // If not present, return an empty list
        return std::map<std::string, TaskExecutor &>{};
    }
    // Otherwise, load all the tasks associated to the application
    return appIterator->second;
}

void NodeManager::update(TaskLaunchOutcome &data)
{
    std::cout << "Received launch outcome for " << data.getTask().getTaskName() << " of " << data.getTask().getAppName()
              << std::endl;
    std::cout << "Launch was " << (data.isLaunchSuccessful() ? "successful" : "not successful") << std::endl;
    // TODO: Remove entry for the task from the map
}
/* ===== API MANAGEMENT ===== */

void NodeManager::update(ApiCommand &data)
{
    // Make the command accept this node manager to be handled
    // Spawn a new thread calling the accept method in order not to block the underlying API listening thread
    data.accept(*this);
}

void NodeManager::handle(StartAppCommand &command) {
    // Here we should first retrieve the configuration file
    /* And after having opened it, see:
     * - how many tasks the app is composed of
     * - how many resources we need to execute each of them
    */
    // First we open the configuration file of the app, by using the provided path
    YAML::Node config = YAML::LoadFile(
            command.getAppLocation() + "config.yaml");
    if (!config["app_name"]) {
        throw std::runtime_error("Config file does not define app's name");
    }
    // Then check whether tasks are defined
    if (!config["tasks"]) {
        throw std::runtime_error("Config file does not define app's tasks");
    }
    // Create an object representing the task in the system
    std::string appName = config["app_name"].as<std::string>();
    // First check whether an app launcher already exists
    auto appIterator = appLaunchers.find(appName);
    if (appIterator != appLaunchers.end()) {
        // TODO: Check whether to return an error message
        // If the app is already pending, stop here
        return;
    }
    // Create a list of tasks composing the app
    std::vector<TaskView> appTasks{};
    auto tasksIterator = appTasks.begin();
    // Get a reference to the list
    auto tasks = config["tasks"];
    // And iterate over it
    for (auto taskIterator = tasks.begin(); taskIterator != tasks.end(); taskIterator++) {
        std::string taskName = (*taskIterator)["task_name"].as<std::string>();
        std::string codeDir = command.getAppLocation() + (*taskIterator)["main_dir"].as<std::string>();
        std::string codeFile = (*taskIterator)["main_file"].as<std::string>();
        int taskReplica = (*taskIterator)["task_count"].as<int>();
        TaskView task(
                appName,
                taskName,
                taskReplica,
                codeDir,
                codeFile);
        task.setCpuQuota((*taskIterator)["cpu_quota"].as<double>());
        task.setFpgaQuota((*taskIterator)["fpga"].as<int>());
        // Append the task view to the list
        appTasks.insert(tasksIterator, task);
        tasksIterator++;
    }
    // Then create an app launcher with all the tasks
    auto launcher = new AppLauncher(appTasks, networkManager.getPeersCount(), nodeId);
    launcher->setNetworkManager((NetworkManagerDelegate *) this);
    // Add it to the table
    appLaunchers.insert(std::pair<std::string, AppLauncher *>(appName, launcher));
    // And make it launch the app
    launcher->launchApp();
}

void NodeManager::handle(KillAppCommand &command)
{
    // Create the event to kill the app
    auto killAppEvent = new KillApp(this->nodeId, command.getAppId());
    // And send it to other nodes in the network as a broadcast message
    this->networkManager.sendEvent(killAppEvent, std::list<std::string>());
}

void NodeManager::handle(RestartAppCommand &command)
{
    // TODO: First kill the app execution
    // TODO: Then handle a new app launch routine
    // TODO: Consider whether to add a new event to restart the app
}

void NodeManager::handle(SetOutCollectionCommand &command) {
    // TODO: Implement this method
}

void NodeManager::handle(ConsumeOutputCommand &command) {
    // TODO: Implement this method
}

void NodeManager::handle(TransferFileCommand &command) {
    PlasterEvent *fileRequestEvent = new FileRequestEvent(nodeId, command.getFilePath(), command.getFileIdentifier());
    auto timesIterator = transferTimes.find(command.getFileIdentifier());
    if(timesIterator != transferTimes.end()){
        transferTimes.erase(timesIterator);
    }
    transferTimes.insert(std::pair<std::string, std::chrono::steady_clock::time_point>(command.getFileIdentifier(), std::chrono::steady_clock::now()));
    networkManager.sendEvent(fileRequestEvent, std::list<std::string>{command.getOwner()});
}

void NodeManager::handle(FileRequestEvent &event) {
    printf("Received file transfer request for %s", event.getFilePath().c_str());
    FILE *fd = fopen(event.getFilePath().c_str(), "rb");
    size_t rret, wret;
    int bytes_read;
    char buffer[FileChunkEvent::CHUNK_SIZE];
    std::list<std::string> target{event.getSender()};
    if (fd == NULL) {
        std::cout << "CANNOT OPEN FILE " << event.getFilePath() << std::endl;
        return;
    }
    while (!feof(fd)) {
        if ((bytes_read = fread(&buffer, 1, FileChunkEvent::CHUNK_SIZE, fd)) > 0) {
            std::string chunkData = buffer;
            PlasterEvent *chunk = new FileChunkEvent(nodeId, event.getFileIdentifier(), chunkData,
                                                     bytes_read < FileChunkEvent::CHUNK_SIZE);
            networkManager.sendEvent(chunk, target);
        } else
            break;
    }
    fclose(fd);
}

void NodeManager::handle(FileChunkEvent &event) {
    std::ofstream outFile;
    outFile.open(event.getFileIdentifier(), std::ios_base::app);
    outFile << event.getChunkData().c_str();
    if (event.isLast()) {
        auto end = std::chrono::steady_clock::now();
        auto timesIterator = transferTimes.find(event.getFileIdentifier());
        std::cout << "Transfer time " << std::chrono::duration_cast<std::chrono::microseconds>(end - timesIterator->second).count() << " us " << std::endl;
    }
    outFile.close();
}

void NodeManager::handle(TaskCommunication &communication)
{
    // We should first check whether the app is being executed here
    auto appIterator = executionPool.find(communication.getAppName());
    if (appIterator == executionPool.end())
    {
        // Do nothing, since the app is not being executed here
        return;
    }
    auto taskIterator = appIterator->second.find(communication.getTaskName());
    if (taskIterator == appIterator->second.end())
    {
        // Do nothing, since the task is not being executed here
        return;
    }
    // Eventually notify the executor about the incoming message
    taskIterator->second.notifyTaskMessage(communication);
}

void NodeManager::onMessageReceived(std::string sender, std::string messageType, std::string messagePayload) {
    // Nothing
}

// These methods just mirror the ones provided by the network manager

void NodeManager::broadcastEvent(PlasterEvent *event) {
    networkManager.broadcastEvent(event);
}

void NodeManager::sendEvent(PlasterEvent *event, std::list<std::string> recipients) {
    networkManager.sendEvent(event, recipients);
}
