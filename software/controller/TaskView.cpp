#include "TaskView.h"

#include <utility>
#include <stdexcept>

BOOST_CLASS_EXPORT_IMPLEMENT(TaskView)

TaskView::TaskView(std::string &appName, std::string &taskName, int replicas, std::string &codeDir,
                   std::string &codeFile) : appName(appName), taskName(taskName),
                                            replicas(replicas),
                                            codeDir(codeDir),
                                            codeFile(codeFile) {
}

const std::string &TaskView::getAppName() const {
    return appName;
}

const std::string &TaskView::getTaskName() const {
    return taskName;
}

const std::string &TaskView::getCodeDir() const {
    return codeDir;
}

const std::string &TaskView::getCodeFile() const {
    return codeFile;
}

int TaskView::getReplicas() const {
    return replicas;
}

TaskView::TaskView(std::string &appName, std::string &taskName, int replicas) :
        appName(appName), taskName(taskName), replicas(replicas), codeDir(std::string()), codeFile(std::string()) {

}

double TaskView::getCpuQuota() const {
    return cpuQuota;
}

void TaskView::setCpuQuota(double cpuQuota) {
    TaskView::cpuQuota = cpuQuota;
}

int TaskView::getFpgaQuota() const {
    return fpgaQuota;
}

void TaskView::setFpgaQuota(int fpgaQuota) {
    TaskView::fpgaQuota = fpgaQuota;
}
