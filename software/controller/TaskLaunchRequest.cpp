#include <stdexcept>
#include "TaskLaunchRequest.h"

TaskLaunchRequest::TaskLaunchRequest(const int requiredNodes, const int peersCount) :
        requiredNodes(requiredNodes),
        peersCount(peersCount),
        availablePeers(),
        availableNodes(0),
        receivedReplies(0) {
    if (requiredNodes < 0 || peersCount < 0) {
        throw std::invalid_argument("Found negative parameters");
    }
}

bool TaskLaunchRequest::addReply(string &peerName, bool isAvailable) {
    if (peerName.empty()) {
        throw std::invalid_argument("Peer name should not be empty");
    }
    // Increase the counter of received replies
    receivedReplies++;
    if (isAvailable) {
        // Get the iterator to the end of the list
        auto tail = availablePeers.end();
        // Insert the new available peer
        availablePeers.insert(tail, peerName);
        // Increase the counter of available nodes
        availableNodes++;
    }
    // And return whether there are enough available nodes to start execution
    return availableNodes >= requiredNodes;
}

void TaskLaunchRequest::updatePeersCount(const int peersCount) {
    if (peersCount < 0) {
        throw std::invalid_argument("Peers count cannot be negative");
    }
    // TODO: Check whether to handle changes in connected peers in another way, by just removing the node and checking
    // if it replied as available
    this->peersCount = peersCount;
    this->availablePeers.empty();
    this->availableNodes = 0;
    this->receivedReplies = 0;
}
