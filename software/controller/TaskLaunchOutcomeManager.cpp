#include "TaskLaunchOutcomeManager.h"

TaskLaunchOutcomeManager::TaskLaunchOutcomeManager(const TaskView &taskView, int nodesCount) :
        BinaryRepliesManager(taskView, nodesCount) {
}

void TaskLaunchOutcomeManager::checkCompletion() {
    // We check whether we received all the required replies
    if (repliesCount == nodesCount) {
        // We first create an object for the outcome by considering the number of successful replies
        TaskLaunchOutcome outcome{taskView, positiveReplies == repliesCount};
        // And send it to attached observers
        notifyObservers(outcome);
    }
}
