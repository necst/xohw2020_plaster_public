#include <stdexcept>
#include "BinaryRepliesManager.h"

BinaryRepliesManager::BinaryRepliesManager(const TaskView &taskView, int nodesCount) : taskView(taskView),
                                                                                       nodesCount(nodesCount) {
    if (nodesCount < 0) {
        throw std::invalid_argument("Number of nodes cannot be negative");
    }
    if (nodesCount < 0) {
        throw std::invalid_argument("Number of nodes cannot be negative");
    }
}

void BinaryRepliesManager::addReply(const std::string &nodeId, const bool isReplyPositive) {
    if (nodeId.empty()) {
        throw std::invalid_argument("Node id cannot be empty");
    }
    // First try to insert the node into the proper set according to its outcome
    if (isReplyPositive) {
        auto insertionResult = positiveNodes.insert(nodeId);
        // Check whether the node was inserted or whether it was already present
        if (insertionResult.second) {
            // Here the node was not present and has been inserted
            // Increment the counter for successful replies
            positiveReplies++;
            repliesCount++;
        }
    } else {
        auto insertionResult = negativeNodes.insert(nodeId);
        // Check whether the node was inserted or whether it was already present
        if (insertionResult.second) {
            // Here the node was not present and has been inserted
            // Increment the counter for successful replies
            repliesCount++;
        }
    }
    // Eventually check whether the end condition is met
    checkCompletion();
}
