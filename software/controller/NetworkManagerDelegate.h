#ifndef PLASTER_NETWORKMANAGERDELEGATE_H
#define PLASTER_NETWORKMANAGERDELEGATE_H

#include <list>

#include "../events/PlasterEvent.h"

/**
 * An interface describing an object that can act on behalf of a {@link NetworkManager} to send events in the network
 */
class NetworkManagerDelegate {
public:
    virtual void broadcastEvent(PlasterEvent *event) = 0;

    virtual void sendEvent(PlasterEvent *event, std::list<std::string> recipients) = 0;
};


#endif //PLASTER_NETWORKMANAGERDELEGATE_H
