

#ifndef PLASTER_BINARYREPLIESMANAGER_H
#define PLASTER_BINARYREPLIESMANAGER_H


#include <string>
#include <set>
#include "TaskView.h"

/**
 * An abstract class used to describe an object that keeps track of binary replies that can be
 * received from other nodes, providing methods to store them, retrieve the peers that replied
 * either positively or negatively and trigger some response when all the required replies
 * hav been received.
 */
class BinaryRepliesManager {
public:
    /**
     * Creates a new instance of the manager for the provided task,
     * expecting a reply from the provided number of nodes
     * @param taskView the object representing the task's information
     * @param nodesCount the number of nodes that are expected to reply, not negative
     */
    BinaryRepliesManager(const TaskView &taskView, int nodesCount);

    /**
     * Stores the reply of the provided node
     * @param nodeId the identifier of the node, not empty
     * @param isLaunchSuccessful {@code true} if the reply is positive, {@code false} otherwise
     */
    void addReply(const std::string &nodeId, bool isReplyPositive);

    /**
     * Checks whether all the needed replies have been received and acts accordingly
     */
    virtual void checkCompletion() = 0;

protected:
    const TaskView taskView;
    /**
     * The count of nodes that were requested to reply
     */
    const int nodesCount;
    /**
     * The number of nodes that replied with a positive reply
     */
    int positiveReplies{0};
    /**
     * The total number of received replies, no matter the type
     */
    int repliesCount{0};

    /**
     * The set of nodes that replied positively
     */
    std::set<std::string> positiveNodes{};
    /**
     * The set of nodes that replied negatively
     */
    std::set<std::string> negativeNodes{};

    //TODO: Check whether to keep a flag indicating when we receive the last reply
};


#endif //PLASTER_BINARYREPLIESMANAGER_H
