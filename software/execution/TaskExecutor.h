#ifndef PLASTER_TASKEXECUTOR_H
#define PLASTER_TASKEXECUTOR_H

#include <thread>
#include "../controller/TaskView.h"
#include "../network/NetworkManager.h"
#include "../events/TaskCommunication.h"

#include <boost/python.hpp>
#include <boost/filesystem.hpp>

namespace bp = boost::python;
using namespace boost::filesystem;

/**
 * A class representing an object that handles the execution of a task
 */
class TaskExecutor {

public:
    /**
     * Creates an instance of the executor to run the provided task
     * @param task the object representing the task's information
     * @param nodeId the identifier of the node executing the task
     */
    explicit TaskExecutor(const TaskView &task, std::string &nodeId);

    /**
     * Starts the execution of the corresponding task
     */
    void start();

    /**
     * Stops the execution of the corresponding task
     */
    void stop();

    /**
     * Retrieves the corresponding task that is being executed
     * @return the object representing the task's information
     */
    const TaskView &getTask() const;

    void setNetworkManager(NetworkManager *networkManager);

    /**
     * Notifies the task that is being executed of an incoming message
     * @param message the object representing the message
     */
    void notifyTaskMessage(TaskCommunication &message);

    void sendTaskMessage(std::string &targetTask, std::string &messageType, std::string &messagePayload);

    // TODO: Add methods provided to tasks

private:
    TaskView task;
    std::string nodeId;
    NetworkManager *networkManager;
    std::atomic<bool> isRunning{true};

    /**
     * The object representing the python-defined task being executed
     */
    bp::object executedTask;

    std::thread executionThread;

    /**
     * The core method of the execution thread
     */
    void runner();

};


#endif //PLASTER_TASKEXECUTOR_H
