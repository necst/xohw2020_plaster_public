#include "TaskExecutor.h"
#include "../api/TaskWrapper.h"

using namespace boost::python;

/**
 * A class wrapping a pointer to a TaskExecutor instance used to provide methods to interact with the cluster
 * to the task that is currently being executed
 */
class ExecutorWrapper {
private:
    TaskExecutor *executor;

public:
    /**
     * Sets the internal reference to point to the provided executor
     * @param executorPointer the pointer to the object representing the executor
     */
    void registerExecutor(TaskExecutor *executorPointer) {
        this->executor = executorPointer;
    }

    void sendMessage(char *targetTask, char *messageType, char *messagePayload) {
        std::string s1(targetTask);
        std::string s2(messageType);
        std::string s3(messagePayload);
        this->executor->sendTaskMessage(s1, s2, s3);
    }
    // TODO: Add facade methods to interact with the executor
};

typedef boost::shared_ptr<ExecutorWrapper> wrapper_ptr;

BOOST_PYTHON_MODULE (ExecutionAPI) {

    class_<TaskWrapper, boost::noncopyable>("PlasterTask")
            .def("run", pure_virtual(&Task::run))
            .def("setup", pure_virtual(&Task::setup))
            .def("on_message_received", pure_virtual(&Task::onMessageReceived))
            .def_readonly("isAlive", &Task::isAlive);

    class_<ExecutorWrapper>("ExecutorWrapper")
            .def("send_message", &ExecutorWrapper::sendMessage);
    // TODO: Expose the facade methods once added
}

bp::object import(const std::string &module, const std::string &path, bp::object &globals) {
    bp::dict locals;
    locals["module_name"] = module;
    locals["path"] = path;

    bp::exec("import imp\n"
             "new_module = imp.load_module(module_name, open(path), path, ('py', 'U', imp.PY_SOURCE))\n",
             globals,
             locals);
    return locals["new_module"];
}

TaskExecutor::TaskExecutor(const TaskView &task, std::string &nodeId) : task(task), nodeId(nodeId) {
}

void TaskExecutor::start() {
    this->isRunning = true;
    // Spawn the execution thread
    this->executionThread = std::thread(&TaskExecutor::runner, this);
}

void TaskExecutor::stop() {
    if (!this->isRunning) {
        return;
    }
    this->isRunning = false;
    if (this->executionThread.joinable()) {
        this->executionThread.join();
    }
}

void TaskExecutor::runner() {

    Py_Initialize();

    try {
        // First create a new wrapper for the executor and put it inside a pointer
        wrapper_ptr wrapperPointer(new ExecutorWrapper);
        wrapperPointer.get()->registerExecutor(this);
        // Initialize the exposed Python module
        initExecutionAPI();
        // Then start importing the Python file
        bp::object main = import("__main__");
        bp::object globals = main.attr("__dict__");
        bp::object module = import("task", (task.getCodeDir() + "/" + task.getCodeFile()), globals);

        // Retrieve the object representing the task in the Python code
        executedTask = module.attr("NodeTask")();
        // Register the executor wrapper to be later used by the task
        executedTask.attr("register")(
                boost::python::ptr(wrapperPointer.get())
        );
        // Call the setup method
        executedTask.attr("setup")();
        // And eventually run it
        executedTask.attr("run")();
        // Then finalize the Python environment once the execution is over
        //Py_Finalize();
    } catch (boost::python::error_already_set &) {
        std::cout << "caught exception from Python" << std::endl;
        PyErr_Print();
    }
}

const TaskView &TaskExecutor::getTask() const {
    return task;
}

void TaskExecutor::setNetworkManager(NetworkManager *networkManager) {
    this->networkManager = networkManager;
}

void TaskExecutor::sendTaskMessage(std::string &targetTask, std::string &messageType, std::string &messagePayload) {
    // Create a new object representing the message, storing the provided type and payload
    TaskCommunication *message = new TaskCommunication(nodeId, messageType, messagePayload);
    std::cout << "Dispatching message from task " 
                << task.getTaskName() 
                << " to task "
                << targetTask
                << " of type " 
                << messageType
                << std::endl;
    // Set the name of the app and the task it is related to
    message->setTaskName(targetTask);
    message->setAppName(task.getAppName());
    // And eventually dispatch it as a broadcast event
    networkManager->sendEvent((PlasterEvent *) message, std::list<std::string>());
}

void TaskExecutor::notifyTaskMessage(TaskCommunication &message) {
    // We notify the task being executed about the incoming message
    executedTask.attr("on_message_received")(message.getSender(), message.getMessageType(),
                                             message.getMessagePayload());
}

