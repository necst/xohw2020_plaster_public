#include "controller/NodeManager.h"
#include "api/PlasterAPI.h"
#include "api/StartAppCommand.h"
#include "Main.h"
#include <string.h>
#include <boost/serialization/export.hpp>
#include <boost/serialization/assume_abstract.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/make_shared.hpp>
#include <boost/serialization/shared_ptr.hpp>


int main() {
    std::string name;
    std::string networkInterface;
    std::string configFile;
    std::cout << "Type node manager name: " << std::endl;
    getline(cin, name);
    std::cout << "Specify the network interface to use (leave blank to use default): " << std::endl;
    getline(cin, networkInterface);
    std::cout << "Specify the node configuration file" << std::endl;
    getline(cin, configFile);

    NodeManager node(name, "node_manager", "PLASTER", networkInterface, configFile);
    node.run();

    while (true) {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        std::cout << "C++ alive" << std::endl;
    }
}
