#ifndef PLASTER_OBSERVABLE_H
#define PLASTER_OBSERVABLE_H

#include <set>
#include <memory>
#include "Observer.h"

template<class T>
/**
 * A class describing an object that can be observed and that notifies its observers about certain events
 * @tparam T The type of data describing the events
 */
class Observable {
public:

    /**
     * Remove the provided observer
     * @param observer The object representing the observer
     */
    void removeObserver(Observer<T> *observer) {
        observers.erase(observer);
    }

    /**
     * Add the provided observer
     * @param observer The object representing the observer
     */
    void addObserver(Observer<T> *observer) {
        observers.insert(observer);
    }

protected:
    /**
     * Notify the observer about an event
     * @param data The data describing the event
     */
    void notifyObservers(T &data) {
        for (auto &o : observers) {
            o->update(data);
        }
    }

private:
    std::set<Observer<T> *> observers;
};

#endif //PLASTER_OBSERVABLE_H
