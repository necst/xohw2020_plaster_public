#ifndef PLASTER_OBSERVER_H
#define PLASTER_OBSERVER_H

template<class T>
/**
 * A class describing an object that observes another object and
 * listens for its updates
 * @tparam T The type of data contained in the updates
 */
class Observer {
public:
    /**
     * Triggered by the observed object when an event occurs
     * @param data The object representing data about the event
     */
    virtual void update(T &data) = 0;
};

#endif //PLASTER_OBSERVER_H
