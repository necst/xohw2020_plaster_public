# README #

### Team informations ###

Team number: xohw20_400  
Project name: Plaster  
Date: June 30th, 2020  
Version of uploaded archive: 1.0
 
### Participants ###

University name: Politecnico di Milano  
Supervisor name: Marco D. Santambrogio  
Supervisor e-mail: marco.santambrogio@polimi.it  
Participant: Daniele Valentino De Vincenti   
Email: danielevalentino.devincenti@mail.polimi.it  
Participant: Lorenzo Farinelli  
Email: lorenzo.farinelli@mail.polimi.it  
Participant: Luca Stornaiuolo  
Email: luca.stornaiuolo@polimi.it

### Project ###

Board used: PYNQ-Z1
Software Version: 1.0
Brief description of project: 

### Description ###
The project is composed of two parts: a C++ application running on the ARM core of the board that orchestrates and manages the cluster infrastructure, enabling the execution of distributed applications on the several boards composing it, and an hardware implementation of the YOLO neural network to perform image detection, which is used by a Python application, developed using the APIs provided by the C++ service, to detect abandoned objects in public places, analyzing the video feed coming from security cameras in a distributed system of FPGA devices, where the computation is split among the several nodes using the map-reduce approach.  

Link to project repository: https://bitbucket.org/necst/pynq-cluster/  

Description of archive (explain directory structure, documents and source files):  

The archive contains the source code of the C++ application managing the cluster, as a Cmake project, and the C++ code which has been accelerated using Vivado HLS to generate the bitstream to load on the board.

### Instructions to build and test project ###
### Step 1: ###
Install the required dependencies for the C++ application, namely  
- libboost-python-dev package  
- Boost libraries (for serialization and filesystem)  
- Zyre framework, using the instructions reported in the library's page: https://github.com/zeromq/zyre#building-on-linux-and-macos
### Step 2: ###
Create a build directory and build the project using cmake:  
`cmake .. && make`  
This will output either an executable file, the actual orchestration application, and a library to be imported in Python containing the APIs to connect to the cluster and perform operations on it (launch app, stop app, ...  
### Step 3 ###
Create a .yaml file describing the resources available on the node, with the following sample syntax
```yaml
resources:
    cpu: 2000
    fpga: 1
```
which should be provided to the application when launching it
### Step 4 ###
To design an app for the cluster, create a folder in the node containing a .yaml configuration file and the python files for the tasks:
The config file has the following sample syntax
```yaml
app_name: "user-defined app"
tasks:
    - task_name: "task1"
      main_dir: "."
      main_file: "test.py"
      task_count: 1
      cpu_quota: 700
      fpga: 0
```
while the python files for the tasks should extend the base class as follows:
``` python
from ExecutionAPI import PlasterTask, ExecutorWrapper

class NodeTask(PlasterTask):

    def __init__(self):
        super(NodeTask, self).__init__()
        self.executor = None

    def setup(self):
        pass

    def run(self):
        pass

    def register(self, ex):
        print("Registering executor")
        self.executor = ex

    def on_message_received(self, sender, message_type message_8payload):
        pass
```


Link to YouTube Video(s):  https://youtu.be/93q-UO_UOXk
